package driver;

import java.util.ArrayList;

/**
 * 
 * This class create files and add contents to the file
 * 
 */
public class Echo extends AbstractCommand{
  
  /**
   * Creates a Echo object.
   * 
   * @param command Command to be created.
   * @param lst List containing arguments.
   */
  public Echo(String command, ArrayList<String> lst) {
    super(command, lst);
  }
  
  /**
   * Checks if argument(s) are valid.
   * 
   * @param current working directory
   * @return if the argument is valid or not
   */
  public Boolean isValidArgument(Directory current)
  // check if the form of the argument is valid
  {
    // no argument
    if(getArgument().size()==1)
    {
      String s = getArgument().get(0);
      if(s.substring(0, 1).equals("\"") 
          & s.substring(s.length()-1).equals("\"")){
        return true;
      }
      System.out.println("Need quatation marks");
      return false;
    }
    System.out.println("The argument is not valid");
    return false;
  }

  
  /**
   * Executes the cat command
   * 
   * @param root Root directory object.
   */
  public String execute()
  {
    // remove the quatation marks
    String s = getArgument().get(0).
        substring(1, getArgument().get(0).length()-1);
    return s;
  }
  
}
