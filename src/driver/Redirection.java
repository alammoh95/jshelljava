package driver;

import java.util.ArrayList;

/**
 * 
 * This class is used to handle the redirection cases
 * 
 */
public class Redirection {
  
  //output of the command
  private String output;
  //either > or >>
  private String arrow;
  //the path of file that needs to be created
  private ArrayList<String> pathDirectory;

  /**
   * Creates a Redirection object.
   * 
   * 
   * @param s output of the command
   * @param a arrow either > or >>
   * @param p the path of file that needs to be created.
   */
  public Redirection(String o, String a, ArrayList<String> p){
    output = o;
    arrow = a;
    pathDirectory = p;
  }
  
  /**
   * Execute the redirection
   * @param root Root directory
   * 
   */
  public void execute(Directory root)
  {
    if(pathDirectory.size()==0){
      System.out.println("Invalid path");
    }else{
      int x = 0;
      int m = 1;
      Directory r = root;
      // check if path exists
      while (x < pathDirectory.size()-1) {
        m = 0;
        int y = 0;
        int n = 0;
        while (y < r.getChildren().size()) {
          n = 0;
          if (pathDirectory.get(x).equals(r.getChildren().get(y).getName())
              & r.getChildren().get(y) instanceof Directory) {
            r = (Directory) r.getChildren().get(y);
            n = 1;
            break;
          }
          n = 0;
          y++;
        }
        if (n == 0) {
          m = 0;
          System.out.println("Path not found");
          break;
        }
        m = 1;
        x++;
      }
      // path found
      if (m == 1) {
        int i=0;
        while(i<r.getChildren().size()){
          if (pathDirectory.get(pathDirectory.size()-1).
              equals(r.getChildren().get(i).getName())
              & r.getChildren().get(i) instanceof File) {
            if(arrow.equals(">")){
              ((File) r.getChildren().get(i)).setContent(output);
              break;
            }else{
              ((File) r.getChildren().get(i)).addContent(output);
              break;
            }
          }
          i++;
        }
        if(i==r.getChildren().size()){
          File f = new File(((String)pathDirectory.
              get(pathDirectory.size()-1)));
          f.setContent(output);
          r.addFile(f);
        }
      }
    }
  }
  
  /**
   * Return the output that needs to be added to the file
   * 
   * @return the output
   */
  public String getOutput(){
    return output;
  }
  
  /**
   * Return the arrow
   * 
   * @return the arrow
   */
  public String getArrow(){
    return arrow;
  }
  
  /**
   * Return the path of the file
   * 
   * @return the path of the file
   */
  public ArrayList<String> getPathDirectory(){
    return pathDirectory;
  }
}
