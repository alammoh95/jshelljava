package driver;

import java.util.ArrayList;

/**
 * 
 * This class can remove a path
 * 
 */
public class Rm extends AbstractCommand {

  // List of ArrayList of strings of directories along the path
  private ArrayList<ArrayList<String>> listPathDirectory = 
      new ArrayList<ArrayList<String>>();
  
  /**
   * Creates a Rm object.
   * 
   * @param command Command to be created.
   * @param lst List containing arguments.
   */
  public Rm(String command, ArrayList<String> lst) {
    super(command, lst);
  }
  
  /**
   * Checks if argument(s) are valid.
   * 
   * @param current working directory
   * @return if the argument is valid or not
   */
  public Boolean isValidArgument(Directory current)
  // check if the form of the argument is valid
  {
    if(getArgument().size() == 0){
      System.out.println("The argument is not valid");
      return false;
    }else if(getArgument().size() == 1){
      ArrayList<String> lst = new ArrayList<String>();
      listPathDirectory.add(lst);
      if (isValidPath(getArgument().get(0), 
          listPathDirectory.get(0), current) == false) {
        System.out.println("The argument is not valid");
        return false;
      }
    }else{
      // add empty lists equal to the number of arguments to directory list
      for (int i = 0; i < getArgument().size(); i++) {
        ArrayList<String> lst = new ArrayList<String>();
        listPathDirectory.add(lst);
      }
      // check if each argument is valid
      for (int i = 0; i < getArgument().size(); i++) {
        String s = getArgument().get(i);
        if (isValidPath(s, listPathDirectory.get(i), current) == false) {
          System.out.println("The argument is not valid");
          return false;
        }
      }
    }
    return true;
  }
  
  /**
   * Return the list of ArrayList of directory along the path.
   * 
   * 
   * @param None
   */
  public ArrayList<ArrayList<String>> getListPathDirectory()
  // get the list of directory in order
  {
    return listPathDirectory;
  }
  
  /**
   * Set the list of ArrayList of directory along the path to lst
   * 
   * 
   * @param lst the new list of list of directory along the path
   */
  public void setListPathDirectory(ArrayList<ArrayList<String>> lst)
  // get the list of directory in order
  {
    listPathDirectory = lst;
  }
  
  /**
   * Find the file by using list of directory along the path
   * 
   * @param root Root directory
   * @param current Current working directory
   * @param pathDirectory list if directory along the path
   * @return the founded file or directory
   */
  public FileName findFile(Directory root, Directory current, 
      ArrayList<String> pathDirectory)
  {
    ArrayList<String> lst = new ArrayList<String>();
    Pwd pwd = new Pwd("pwd", lst);
    String path = pwd.execute(current);
    ArrayList<String> pathDirectory3 = new ArrayList<String>();
    Boolean a = isValidPath(path, pathDirectory3, current);
    Boolean b = true;
    if(pathDirectory.size()==0 & current.getName().equals(root.getName()))
    {
      return root;
    }else if (pathDirectory3.size() >= pathDirectory.size()) {
      int i = 0;
      while (i < pathDirectory.size()) {
        if (!pathDirectory.get(i).equals(pathDirectory3.get(i))) {
          break;
        }
        i++;
      }
      // remove a directory that is currently working is not valid
      if (i == pathDirectory.size()) {
        b = false;
        System.out.println("Invalid remove");
        return null;
      }
    }
    if(b==true){
      int x = 0;
      int m = 1;
      // check if path exists
      while (x < pathDirectory.size()-1) {
        m = 0;
        int y = 0;
        int n = 0;
        while (y < root.getChildren().size()) {
          n = 0;
          //directory
          if (pathDirectory.get(x).equals
              (root.getChildren().get(y).getName())
              & root.getChildren().get(y) instanceof Directory) 
          {
            root = (Directory) root.getChildren().get(y);
            n = 1;
            break;
          }
          n = 0;
          y++;
        }
        if (n == 0) {
          m = 0;
          System.out.println("Path not found");
          return null;
        }
        m = 1;
        x++;
      }
      // path found
      if (m == 1) {
        Boolean aa = false;
        int i1=0;
        while(i1<root.getChildren().size())
        {
          if(root.getChildren().get(i1).getName().
              equals(pathDirectory.get(pathDirectory.size()-1)))
          {
            aa = true;
            return root.getChildren().get(i1);
          }
          i1++;
        }
        if(aa == false){
          System.out.println("File or directory not found");
          return null;
        }
      }
    }
    return null;
  }
  
  /**
   * Recursively add all list of directories and files path in directory or file f 
   * to the list
   * 
   * @param f Directory or file f
   * @param lst list that is going to accept lists of path directories
   * @param current Current working directory
   */
  public void recursiveRm(FileName f, ArrayList<ArrayList<String>> lst, 
      Directory current)
  {
    ArrayList<String> l = new ArrayList<String>();
    Pwd pwd = new Pwd("pwd", l);
    ArrayList<String> pathDirectory = new ArrayList<String>();
    if(!f.getParentDirectory().getName().equals("")){
      String path = pwd.execute(f.getParentDirectory());
      if(path.equals("/")){
        path = path + f.getName();
      }else{
        path = path + "/" + f.getName();
      }
      isValidPath(path, pathDirectory, current);
    }
    lst.add(pathDirectory);
    if (f instanceof Directory && ((Directory) f).getChildren().size() > 0){
      for(int i = 0; i < ((Directory) f).getChildren().size(); i++){
        recursiveRm(((Directory) f).getChildren().get(i), lst, current);
      }
    }
  }
  
  /**
   * Remove the path directory
   * 
   * @param root Root directory.
   * @param current Current working directory
   * @param a list of directory of one path
   */
  public String execute(Directory root, Directory current, 
      ArrayList<String> pathDirectory) {
    Directory root1 = root;
    Directory current1 = current;
    ArrayList<String> lst = new ArrayList<String>();
    Pwd pwd = new Pwd("pwd", lst);
    String path = pwd.execute(current1);
    ArrayList<String> pathDirectory3 = new ArrayList<String>();
    Boolean a = isValidPath(path, pathDirectory3, current1);
    Boolean b = true;
    // remove all directories in the root
    if(pathDirectory.size()==0 & current1.getName().equals(root1.getName()))
    {
      b = false;
      root1.getChildren().clear();
    }else if (pathDirectory3.size() >= pathDirectory.size()) {
      int i = 0;
      while (i < pathDirectory.size()) {
        if (!pathDirectory.get(i).equals(pathDirectory3.get(i))) {
          break;
        }
        i++;
      }
      // remove a directory that is currently working is not valid
      if (i == pathDirectory.size()) {
        b = false;
        return "Invalid remove";
      }
    }
    if(b==true){
      int x = 0;
      int m = 1;
      // check if path exists
      while (x < pathDirectory.size()-1) {
        m = 0;
        int y = 0;
        int n = 0;
        while (y < root1.getChildren().size()) {
          n = 0;
          //directory
          if (pathDirectory.get(x).equals
              (root1.getChildren().get(y).getName())
              & root1.getChildren().get(y) instanceof Directory) 
          {
            root1 = (Directory) root1.getChildren().get(y);
            n = 1;
            break;
          }
          n = 0;
          y++;
        }
        if (n == 0) {
          m = 0;
          return "Path not found";
        }
        m = 1;
        x++;
      }
      // path found
      if (m == 1) {
        Boolean aa = false;
        int i1=0;
        while(i1<root1.getChildren().size())
        {
          if(root1.getChildren().get(i1).getName().
              equals(pathDirectory.get(pathDirectory.size()-1)))
          {
            aa = true;
            root1.getChildren().remove(i1);
            break;
          }
          i1++;
        }
        if(aa == false){
          return "File or directory not found";
        }
      }
    }
    return null;
  }
  
}
