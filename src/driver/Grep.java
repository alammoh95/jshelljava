package driver;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * 
 * This class lists the items in a directory
 * 
 */
public class Grep extends AbstractCommand {

  //List of ArrayList of strings of directories along the path
  private ArrayList<ArrayList<String>> listPathDirectory = 
      new ArrayList<ArrayList<String>>();
  //List of all contents that is going to be searched
  private ArrayList<String> allContent = new ArrayList<String>();
  // REGEX
  private String REGEX;

  /**
   * Creates a Grep object.
   * 
   * @param command Command to be created.
   * @param lst List containing arguments.
   */
  public Grep(String command, ArrayList<String> lst) {
    super(command, lst);
    if(lst.size()>1){
      REGEX = lst.get(0);
    }
  }

  /**
   * Checks if argument(s) are valid.
   * 
   * @param current working directory
   * @return if the argument is valid or not
   */
  public Boolean isValidArgument(Directory current)
  // check if the form of the argument is valid
  {
    // invalid number of arguments
    if (getArgument().size() == 0 | getArgument().size() == 1) {
      System.out.println("The argument is not valid");
      return false;
    }else{
      // add empty lists equal to the number of arguments-1 to directory list
      for (int i = 0; i < getArgument().size()-1; i++) {
        ArrayList<String> lst = new ArrayList<String>();
        listPathDirectory.add(lst);
      }
      // check if each argument is valid
      for (int i = 1; i < getArgument().size(); i++) {
        String s = getArgument().get(i);
        if (isValidPath(s, listPathDirectory.get(i-1), current) == false) {
          System.out.println("The argument is not valid");
          return false;
        }
      }
      try {
        Pattern.compile(REGEX);
      }catch (PatternSyntaxException exception) {
        System.out.println("The REGEX is not valid");
        return false;
      }
    }
    return true;
  }
  
  /**
   * Returns list of ArrayList of directory along the path.
   * 
   * 
   * @param None
   * @return the list of ArrayList of directory along the path.
   */
  public ArrayList<ArrayList<String>> getListPathDirectory()
  // get the list of directory in order
  {
    return listPathDirectory;
  }
  
  /**
   * Return list of all content.
   * 
   * @return list of all content.
   */
  public ArrayList<String> getAllContent()
  // get the list of directory in order
  {
    return allContent;
  }
  
  /**
   * Recursively find all files in d and add content to the list
   * 
   * @param d Directory or file
   */
  public void recursiveAddFileContent(FileName f){
    if(f instanceof File){
      for(int i=0; i<((File) f).getContent().size(); i++)
      {
        if(!f.getParentDirectory().getName().equals(""))
        {
          Directory parent = f.getParentDirectory();
          ArrayList<String> lst = new ArrayList<String>();
          Pwd pwd = new Pwd("pwd", lst);
          String path = pwd.execute(parent);
          if(path.equals("/")){
            path = path + f.getName();
          }else{
            path = path + "/" + f.getName();
          }
          path = path + ": ";
          allContent.add(path + ((File) f).getContent().get(i));
        }
      }
    }else{
      for(int i=0; i<((Directory) f).getChildren().size(); i++)
      {
        recursiveAddFileContent(((Directory) f).getChildren().get(i));
      }
    }
  }
  
  /**
   * Add the content of the file to the list allContent
   * 
   * @param f The file
   */
  public void addFileContent(File f){
    for(int i=0; i<f.getContent().size(); i++)
    {
      allContent.add(f.getContent().get(i));
    }
  }
  
  /**
   * Return the lines of content that contain the match of the regex
   * 
   * @param r Whether findRegex for recursive content or not
   */
  public String findRegex(Boolean r){
    String s = "";
    Pattern pattern = Pattern.compile(REGEX);
    for(int i=0; i<allContent.size(); i++)
    {
      String content = allContent.get(i);
      if(r==true){
        for(int j=0; j<allContent.get(i).length(); j++){
          if(Character.toString(allContent.get(i).charAt(j)).equals(":")){
            content = allContent.get(i).
                substring(j+2, allContent.get(i).length());
            break;
          }
        }
      }
      Matcher matcher = pattern.matcher(content);
      if (matcher.find()){
        s = s + allContent.get(i);
        s = s + "\n";
      }
    }
    return s;
  }
  
  /**
   * Execute the grep command
   * 
   * @param root Root directory
   * @param current Current working directory
   * @param r Whether recursive or not
   * @return the line contains the match of REGEX
   */
  public String execute(Directory root, Directory current, Boolean r){
    String s = "";
    // no recursive print
    if(r==false){
      for(int j=0; j<listPathDirectory.size(); j++){
        ArrayList<String> pathDirectory = listPathDirectory.get(j);
        Directory root1 = root;
        //root directory
        if(pathDirectory.size()==0){
          for (int i = 0; i < root1.getChildren().size(); i++) {
            if(root1.getChildren().get(i) instanceof File){
              addFileContent((File) root1.getChildren().get(i));
            }
          }
        }else{
          int x = 0;
          int m = 0;
          int file = 0;
          // check if path exists
          while (x < pathDirectory.size()) {
            m = 0;
            int y = 0;
            int n = 0;
            while (y < root1.getChildren().size()) {
              n = 0;
              //directory
              if (pathDirectory.get(x).equals
                  (root1.getChildren().get(y).getName())
                  & root1.getChildren().get(y) instanceof Directory) 
              {
                root1 = (Directory) root1.getChildren().get(y);
                n = 1;
                break;
              //path indicates a file
              }else if(pathDirectory.get(x).equals
                  (root1.getChildren().get(y).getName())
                  & root1.getChildren().get(y) instanceof File 
                  & x == pathDirectory.size()-1) 
              {
                addFileContent((File) root1.getChildren().get(y));
                file = 1;
                n = 1;
                break;
              }
              n = 0;
              y++;
            }
            if (n == 0) {
              m = 0;
              System.out.println("Path not found");
              break;
            }
            m = 1;
            x++;
          }
          // path found
          if (m == 1 & file == 0) {
            for (int i = 0; i < root1.getChildren().size(); i++) {
              if(root1.getChildren().get(i) instanceof File){
                addFileContent((File) root1.getChildren().get(i));
              }
            }
          }
        }
      }
    // recursive print
    }else{
      for(int j=0; j<listPathDirectory.size(); j++){
        ArrayList<String> pathDirectory = listPathDirectory.get(j);
        // no argument
        Directory root1 = root;
        if(pathDirectory.size()==0){
          recursiveAddFileContent(root1);
        }else{
          int x = 0;
          int m = 0;
          int file = 0;
          // check if path exists
          while (x < pathDirectory.size()) {
            m = 0;
            int y = 0;
            int n = 0;
            while (y < root1.getChildren().size()) {
              n = 0;
              //directory
              if (pathDirectory.get(x).equals
                  (root1.getChildren().get(y).getName())
                  & root1.getChildren().get(y) instanceof Directory) 
              {
                root1 = (Directory) root1.getChildren().get(y);
                n = 1;
                break;
              //path indicates a file
              }else if(pathDirectory.get(x).equals
                  (root1.getChildren().get(y).getName())
                  & root1.getChildren().get(y) instanceof File 
                  & x == pathDirectory.size()-1) 
              {
                recursiveAddFileContent(root1.getChildren().get(y));
                file = 1;
                n = 1;
                break;
              }
              n = 0;
              y++;
            }
            if (n == 0) {
              m = 0;
              System.out.println("Path not found");
              break;
            }
            m = 1;
            x++;
          }
          // path found
          if (m == 1 & file == 0) {
            recursiveAddFileContent(root1);
          }
        }
      }
    }
    return findRegex(r);
  }
  
}
