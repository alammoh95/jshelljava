package driver;

import java.util.ArrayList;

/**
 * 
 * This class lists the items in a directory
 * 
 */
public class Ls extends AbstractCommand {

  // List of ArrayList of strings of directories along the path
  private ArrayList<ArrayList<String>> listPathDirectory = 
      new ArrayList<ArrayList<String>>();

  /**
   * Creates a Ls object.
   * 
   * @param command Command to be created.
   * @param lst List containing arguments.
   */
  public Ls(String command, ArrayList<String> lst) {
    super(command, lst);
  }

  /**
   * Checks if argument(s) are valid.
   * 
   * @param current working directory
   * @return if the argument is valid or not
   */
  public Boolean isValidArgument(Directory current)
  // check if the form of the argument is valid
  {
    // no argument
    if (getArgument().size() == 0) {
      return true;
    }else if(getArgument().size() == 1){
      ArrayList<String> lst = new ArrayList<String>();
      listPathDirectory.add(lst);
      if (isValidPath(getArgument().get(0), 
          listPathDirectory.get(0), current) == false) {
        System.out.println("The argument is not valid");
        return false;
      }
    }else{
      // add empty lists equal to the number of arguments to directory list
      for (int i = 0; i < getArgument().size(); i++) {
        ArrayList<String> lst = new ArrayList<String>();
        listPathDirectory.add(lst);
      }
      // check if each argument is valid
      for (int i = 0; i < getArgument().size(); i++) {
        String s = getArgument().get(i);
        if (isValidPath(s, listPathDirectory.get(i), current) == false) {
          System.out.println("The argument is not valid");
          return false;
        }
      }
    }
    return true;
  }

  /**
   * Returns list of ArrayList of directory along the path.
   * 
   * 
   * @param None
   * @return the list of ArrayList of directory along the path.
   */
  public ArrayList<ArrayList<String>> getListPathDirectory()
  // get the list of directory in order
  {
    return listPathDirectory;
  }

  /**
   * Recursively print all files in directory d
   * 
   * @param d Directory
   * @return all files in d
   */
  public String recursivePrint(Directory d){
    ArrayList<String> lst = new ArrayList<String>();
    Pwd pwd = new Pwd("pwd", lst);
    String path = pwd.execute(d);
    String s = path + ":   ";
    if (d.getChildren().size() == 0) 
    {
      s = s + "\n";
    } else {
      for (int i = 0; i < d.getChildren().size(); i++) 
      {
        s = s + d.getChildren().get(i).getName() + "   ";
      }
      s = s + "\n";
      for (int i = 0; i < d.getChildren().size(); i++) 
      {
        if(d.getChildren().get(i) instanceof Directory)
        {
          //recursive call
          s = s + recursivePrint((Directory)d.getChildren().get(i));
        }else{
          s = s + ((File)d.getChildren().get(i)).getName() + "   " + "\n";
        }
      }
    }
    return s;
  }
  
  /**
   * Execute the ls command and return strings of all files
   * 
   * @param root Root directory.
   * @param current Current directory object.
   * @param r Whether need recursive print or not
   * @return Strings of all files
   */
  public String execute(Directory root, Directory current, Boolean r) {
    String s = "";
    // no recursive print
    if(r==false){
      // no argument
      if (getArgument().size() == 0) {
        for (int i = 0; i < current.getChildren().size(); i++) {
          s = s + current.getChildren().get(i).getName() + "    ";
        }
      }else{
        for(int j=0; j<listPathDirectory.size(); j++){
          ArrayList<String> pathDirectory = listPathDirectory.get(j);
          Directory root1 = root;
          //root directory
          if(pathDirectory.size()==0){
            s = s + "/:" + "   ";
            for (int i = 0; i < root1.getChildren().size(); i++) {
              s = s + root1.getChildren().get(i).getName() + "    ";
            }
          }else{
            int x = 0;
            int m = 0;
            int file = 0;
            // check if path exists
            while (x < pathDirectory.size()) {
              m = 0;
              int y = 0;
              int n = 0;
              while (y < root1.getChildren().size()) {
                n = 0;
                //directory
                if (pathDirectory.get(x).equals
                    (root1.getChildren().get(y).getName())
                    & root1.getChildren().get(y) instanceof Directory) 
                {
                  root1 = (Directory) root1.getChildren().get(y);
                  n = 1;
                  break;
                //path indicates a file
                }else if(pathDirectory.get(x).equals
                    (root1.getChildren().get(y).getName())
                    & root1.getChildren().get(y) instanceof File 
                    & x == pathDirectory.size()-1) 
                {
                  ArrayList<String> lst = new ArrayList<String>();
                  Pwd pwd = new Pwd("pwd", lst);
                  String path = pwd.execute(root1);
                  s = s + ((File) root1.getChildren().get(y)).getName();
                  file = 1;
                  n = 1;
                  break;
                }
                n = 0;
                y++;
              }
              if (n == 0) {
                m = 0;
                System.out.println("Path not found");
                break;
              }
              m = 1;
              x++;
            }
            // path found
            if (m == 1 & file == 0) {
              ArrayList<String> lst = new ArrayList<String>();
              Pwd pwd = new Pwd("pwd", lst);
              String path = pwd.execute(root1);
              s = s + path + ":" + "   ";
              for (int i = 0; i < root1.getChildren().size(); i++) {
                s = s + root1.getChildren().get(i).getName() + "    ";
              }
            }
          }
        }
      }
    // recursive print
    }else{
      //print current directory
      if (getArgument().size() == 0) {
        s = s + recursivePrint(current);
      }else{
        for(int j=0; j<listPathDirectory.size(); j++){
          ArrayList<String> pathDirectory = listPathDirectory.get(j);
          // no argument
          Directory root1 = root;
          if(pathDirectory.size()==0){
            s = s + recursivePrint(root);
          }else{
            int x = 0;
            int m = 0;
            int file = 0;
            // check if path exists
            while (x < pathDirectory.size()) {
              m = 0;
              int y = 0;
              int n = 0;
              while (y < root1.getChildren().size()) {
                n = 0;
                //directory
                if (pathDirectory.get(x).equals
                    (root1.getChildren().get(y).getName())
                    & root1.getChildren().get(y) instanceof Directory) 
                {
                  root1 = (Directory) root1.getChildren().get(y);
                  n = 1;
                  break;
                //path indicates a file
                }else if(pathDirectory.get(x).equals
                    (root1.getChildren().get(y).getName())
                    & root1.getChildren().get(y) instanceof File 
                    & x == pathDirectory.size()-1) 
                {
                  ArrayList<String> lst = new ArrayList<String>();
                  Pwd pwd = new Pwd("pwd", lst);
                  String path = pwd.execute(root1);
                  s = s + ((File) root1.getChildren().get(y)).getName();
                  s = s + "\n";
                  file = 1;
                  n = 1;
                  break;
                }
                n = 0;
                y++;
              }
              if (n == 0) {
                m = 0;
                System.out.println("Path not found");
                break;
              }
              m = 1;
              x++;
            }
            // path found
            if (m == 1 & file == 0) {
              s = s + recursivePrint(root1);
            }
          }
        }
      }
    }
    return s;
  }
  
}
