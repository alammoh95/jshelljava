package driver;

import java.util.ArrayList;

/**
 * This class
 * 
 */
public abstract class AbstractCommand {

  private String command;
  private ArrayList<String> argument;
  // valid character
  private String validPathElement = "abcdefghijklmnopqrstuvwxyz1234567890"
      + "ABCDEFGHIJKLMNOPQRSTUVWXYZ_/.";
  private String validNameElement = "abcdefghijklmnopqrstuvwxyz1234567890"
      + "ABCDEFGHIJKLMNOPQRSTUVWXYZ_.";

  /**
   * Creates an AbstractCommand Object.
   * 
   * 
   * @param s: Command name.
   * @param lst: List of arguments.
   */
  public AbstractCommand(String s, ArrayList<String> lst) {
    command = s;
    argument = lst;
  }
  
  /**
   * Check if given arguments are valid
   * 
   * @param current Current working directory
   * @return True if the argument is valid
   */
  abstract Boolean isValidArgument(Directory current);

  /**
   * Transfer DIR or FILE or PATH to an absolute path
   * 
   * 
   * @param s: A string of DIR or FILE or PATH
   * @param current: current working directory.
   * @return the absolute path
   */
  public String absolutePath(String s, Directory current){
    if(s.equals(".")){
      s = "/";
      ArrayList lst = new ArrayList();
      Pwd pwd = new Pwd("pwd", lst);
      String a = pwd.execute(current);
      if (!a.equals("/")) {
        s = a + s;
      }
    }else if(s.equals("..")){
      if (!current.getParentDirectory().getName().equals("")) {
        current = current.getParentDirectory();
      }else{
        return "";
      }
      s = "/";
      ArrayList lst = new ArrayList();
      Pwd pwd = new Pwd("pwd", lst);
      String a = pwd.execute(current);
      if (!a.equals("/")) {
        s = a + s;
      }
    }else if (s.length() >= 4 && s.substring(0, 3).equals("../")) {
      s = s.substring(2, s.length());
      if (!current.getParentDirectory().getName().equals("")) {
        current = current.getParentDirectory();
      }else{
        return "";
      }
      ArrayList lst = new ArrayList();
      Pwd pwd = new Pwd("pwd", lst);
      String a = pwd.execute(current);
      if (!a.equals("/")) {
        s = a + s;
      }
      // path with .
    }else if (s.length() >= 3 && s.substring(0, 2).equals("./")){
      s = s.substring(1, s.length());
      ArrayList lst = new ArrayList();
      Pwd pwd = new Pwd("pwd", lst);
      String a = pwd.execute(current);
      if (!a.equals("/")) {
        s = a + s;
      }
    }else if(isValidName(s)==true){
      s = "/" + s;
      ArrayList lst = new ArrayList();
      Pwd pwd = new Pwd("pwd", lst);
      String a = pwd.execute(current);
      if (!a.equals("/")) {
        s = a + s;
      }
    }else if(!s.substring(0, 1).equals("/")){
      s = "/" + s;
      ArrayList lst = new ArrayList();
      Pwd pwd = new Pwd("pwd", lst);
      String a = pwd.execute(current);
      if (!a.equals("/")) {
        s = a + s;
      }
    }
    return s;
  }
    
  /**
   * Checks to see if s has a valid form of a path. If s is valid, add all the
   * directories along the path to the list directory.
   * 
   * @param s: A string of path.
   * @param pathDirectory: List containing directories to add s.
   * @param current: Current working directory
   * @param return if the s is valid
   */
  public Boolean isValidPath(String s, ArrayList<String> pathDirectory,
      Directory current)
  // check if String s has a valid format of directory
  {
    for (int i = 0; i < s.length(); i++) {
      if (!validPathElement.contains(Character.toString(s.charAt(i)))) {
        return false;
      }
    }
    s = absolutePath(s, current);
    if(s.equals("")){
      return false;
    }
    if(s.equals("/")){
      return true;
    }else{
      // path starts with /
      if (Character.toString(s.charAt(0)).equals("/")) {
        // no double slash
        if (Character.toString(s.charAt(1)).equals("/")) {
          return false;
        }
        int j = 1;
        for (int i = 1; i < s.length(); i++) {
          if (Character.toString(s.charAt(i)).equals("/")) {
            if (i < s.length() - 1) {
              if (!Character.toString(s.charAt(i + 1)).equals("/")) {
                // get directory from the path and add them to the list
                pathDirectory.add(s.substring(j, i));
                j = i + 1;
                // no double slash
              } else {
                return false;
              }
              // haven't reach the end yet
            } else {
              pathDirectory.add(s.substring(j, i));
              return true;
            }
            // reach the end
          }
          if (i == s.length() - 1) {
            // path ends with /
            if (Character.toString(s.charAt(i)).equals("/")) {
              pathDirectory.add(s.substring(j, i));
              return true;
              // path does not ends with /
            } else {
              pathDirectory.add(s.substring(j, i + 1));
              return true;
            }
          }
        }
        // path does not starts with /
      } else {
        return false;
      }
    }
    // no argument
    return false;
  }

  /**
   * Checks to see if s has a valid form for a file/direcotry name.
   * 
   * 
   * @param s Name to be checked.
   * @return if s is valid name or not
   */
  public Boolean isValidName(String s) {
    if(s.equals(".") | s.equals("..")){
      return false;
    }
    for (int i = 0; i < s.length(); i++) {
      // contains special character
      if (!validNameElement.contains(Character.toString(s.charAt(i)))) {
        return false;
      }
    }
    return true;
  }
  
  /**
   * Get the command
   * 
   * 
   * @param None
   * @return The command
   */
  public String getCommand(){
    return command;
  }
  
  /**
   * Get the argument
   * 
   * 
   * @param None
   * @return The argument
   */
  public ArrayList<String> getArgument(){
    return argument;
  }

  
}
