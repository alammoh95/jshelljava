package driver;

import java.util.ArrayList;

/**
 * 
 * This class represents a File object
 * 
 */
public class File extends FileName {
  
  //a string of file content
  private ArrayList<String> content = new ArrayList<String>();
  /**
   * Creates a File object.
   * 
   * 
   * @param name Name of File.
   */

  public File(String name) {
    super(name);
  }
  
  /**
   * Add content to this file at line i
   * 
   * @param i The line of content to be added
   * @return the content of file
   */
  public void addContent(String s){
    content.add(s);
  }
  
  /**
   * Return the content of this file
   * 
   * 
   * @return the content of file
   */
  // output's file contents
  public ArrayList<String> getContent() {
    return content;
  }
  
  /**
   * Set the content of this file to s
   * 
   * @param s The new content
   */
  public void setContent(String s){
    ArrayList<String> lst = new ArrayList<String>();
    lst.add(s);
    content = lst;
  }
  /**
   * Copy the file
   * 
   * 
   * @param none
   * @return the copy of file
   */

  public File copy() {
    File f = new File(getName());
    for(int i=0; i<content.size(); i++){
      f.content.add(content.get(i));
    }
    return f;
  }

}
