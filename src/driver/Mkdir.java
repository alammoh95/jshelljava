package driver;

import java.util.ArrayList;

/**
 * 
 * This class creates Directories
 * 
 */
public class Mkdir extends AbstractCommand {

  // List of strings of directories along the path
  private ArrayList<ArrayList<String>> listPathDirectory = 
      new ArrayList<ArrayList<String>>();

  /**
   * Creates a Mkdir object.
   * 
   * 
   * @param command Command to be created.
   * @param lst List containing arguments.
   */
  public Mkdir(String command, ArrayList<String> lst) {
    super(command, lst);
  }

  /**
   * Checks if argument(s) for mkdir are valid and print error message if 
   * needed
   * 
   * @param Current working directory
   * @return if the argument is valid or not
   */
  public Boolean isValidArgument(Directory current) {
    // no argument
    if (this.getArgument().size() == 0) {
      System.out.println("Need argument");
      return false;
    }
    // add empty lists equal to the number of arguments to directory list
    for (int i = 0; i < getArgument().size(); i++) {
      ArrayList<String> lst = new ArrayList<String>();
      listPathDirectory.add(lst);
    }
    // check if each argument is valid
    for (int i = 0; i < getArgument().size(); i++) {
      String s = getArgument().get(i);
      if (isValidPath(s, listPathDirectory.get(i), current) == false 
          | s.equals("/")) {
        System.out.println("The argument is not valid");
        return false;
      }
    }
    return true;
  }

  /**
   * Returns Directory of object.
   * 
   * 
   * @param None
   */
  public ArrayList<ArrayList<String>> getlistPathDirectory() {
    return listPathDirectory;
  }

  /**
   * Executes the make directory command.
   * 
   * @param root Root directory.
   * @return None
   */
  public void execute(Directory root) {
    int m = 0;
    // execute the command for each argument
    while (m < getArgument().size()) {
      String a = getArgument().get(m);
      ArrayList<String> directory = listPathDirectory.get(m);
        // mkdir in root directory
      if (directory.size() == 1) {
        Directory d = new Directory(directory.get(0));
        int i = 0;
        while (i < root.getChildren().size()) {
          // directory already exists
          if (d.getName().equals(root.getChildren().get(i).getName())) {
            System.out.println("Directory " + "'" + a + "'" + " in root folder"
                + " already exists");
            break;
          }
          i++;
        }
        if (i == root.getChildren().size()) {
          root.addDirectory(d);
          d.setParentDirectory(root);
        }
        // mkdir in deeper directory
      } else {
        Directory d = root;
        int i = 0;
        int j = 0;
        int b = 0;
        while (i < directory.size() - 1) {
          b = 0;
          j = 0;
          while (j < d.getChildren().size()) {
            // check if the directory on the path exists
            if (directory.get(i).equals(d.getChildren().get(j).getName())
                & d.getChildren().get(j) instanceof Directory) {
              d = (Directory) d.getChildren().get(j);
              j = 0;
              b = 1;
              break;
            }
            b = 0;
            j++;
          }
          // path not found
          if (b == 0) {
            String s = getArgument().get(m);
            int n = getArgument().get(m).length() - 1;
            while (n > 0) {
              if (Character.toString(s.charAt(n)).equals("/")) {
                if (n == getArgument().get(m).length() - 1) {
                  n = n - 1;
                } else {
                  if (Character.toString(s.charAt(n - 1)).equals("/")) {
                    s = s.substring(0, n - 1);
                    break;
                  }
                  n = n - 1;
                }
              } else {
                if (Character.toString(s.charAt(n - 1)).equals("/")) {
                  s = s.substring(0, n - 1);
                  break;
                }
                n = n - 1;
              }
            }
            System.out.println("The path " + "'" + s + "'" + " not found");
            break;
          }
          i++;
        }
        // path found
        if (b == 1) {
          int k = 0;
          if (i == directory.size() - 1) {
            while (k < d.getChildren().size()) {
              if (directory.get(directory.size() - 1).equals(
                  d.getChildren().get(k).getName())
                  & d.getChildren().get(k) instanceof Directory) {
                String s = getArgument().get(m);
                int n = getArgument().get(m).length() - 1;
                // directory already exists
                while (n > 0) {
                  if (Character.toString(s.charAt(n)).equals("/")) {
                    if (n == getArgument().get(m).length() - 1) {
                      n = n - 1;
                    } else {
                      if (Character.toString(s.charAt(n - 1)).equals("/")) {
                        s = s.substring(0, n - 1);
                        break;
                      }
                      n = n - 1;
                    }
                  } else {
                    if (Character.toString(s.charAt(n - 1)).equals("/")) {
                      s = s.substring(0, n - 1);
                      break;
                    }
                    n = n - 1;
                  }
                }
                System.out.println("Directory " + "'" + 
                d.getChildren().get(k).getName()
                    + "'" + " in path " + "'" + s + "'" + " already exists");
                break;
              }
              k++;
            }
          }
          // create the directory
          if (k == d.getChildren().size()) {
            Directory n =
                new Directory(directory.get(directory.size() - 1));
            d.addDirectory(n);
          }
        }
      }
      m++;
    }
  }
}
