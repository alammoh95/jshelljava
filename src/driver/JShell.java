// **********************************************************
// Assignment2:
// UTOR user_name1:chowdhr8
// UTOR user_name2:pushi
// UTOR user_name3: alammoh5
// UTOR user_name4: tahirha3
//
// Author1: Mohammad Chowdhry
// Author2: Shi Pu
// Author3: Mohammad Osama Alam
// Author4: Hassan Ali Tahir
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
//
// Note for marker:
// After discussing with Professor Abbas, we reached an agreement
// that only 8 commands were required to be implemented in order to
// to complete this assignment. Therefore, the 8 commands implemented are:
// cd, cp, mv, ls, man, mkdir, pwd, exit.
// *********************************************************

package driver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.UnknownHostException;
import java.util.ArrayList;
import java.util.Stack;

/**
 * 
 * This class represents the shell of the Mock File System User inputs commands
 * and output is printed from here
 * 
 */
public class JShell {

  public static void main(String[] args) {
    String input = null;
    // using Buffer to read user command inputs
    BufferedReader console =
        new BufferedReader(new InputStreamReader(System.in));
    // root directory
    Directory root = new Directory("/");
    // current directory
    Directory current = root;
    Directory d = new Directory("");
    root.setParentDirectory(d);
    // Stack to hold directories
    Stack<Directory> stack=new Stack<Directory>();
    while (true) {
      System.out.print("/# ");
      try {
        input = console.readLine();
      } catch (IOException e) {
        e.printStackTrace();
      }
      // console exit condition
      if (input.equals("exit"))
        break;
      int length = input.length();
      // the case with input length>0
      String command = "";
      ArrayList<String> argument = new ArrayList<String>();
      if (length > 0) {
        // ignore the space in front of the command
        int i = 0;
        while (i < length) {
          if (!input.substring(i, i + 1).equals(" ")) {
            break;
          }
          i++;
        }
        // input with only space
        if (i == length) {
          System.out.print("");
        } else {
          int j = i;
          // ignore the space after the command
          while (j < length) {
            if (input.substring(j, j + 1).equals(" ")) {
              break;
            }
            j++;
          }
          // get the command
          command = input.substring(i, j);
          // keep adding each argument that separated by space to the list
          while (j < length) {
            if (input.substring(j, j + 1).equals(" ")) {
              j++;
            } else {
              int k = j;
              while (k < length) {
                if (!input.substring(k, k + 1).equals(" ")) {
                  k++;
                } else {
                  break;
                }
              }
              argument.add(input.substring(j, k));
              j = k;
            }
          }
          //check redirection
          // Redirection path
          ArrayList<String> pathDirectoryRedirection = new ArrayList<String>();
          Boolean redirection = false;
          String arrow = "";
          if(argument.size()>=2){
            arrow = argument.get(argument.size()-2);
            String path = argument.get(argument.size()-1);
            Echo a = new Echo(command, argument);
            // check if the redirection argument is valid or not
            if((arrow.equals(">>") | arrow.equals(">")) 
                & a.isValidPath(path, pathDirectoryRedirection, current)){
              redirection = true;
              //remove redirection element
              argument.remove(argument.size()-2);
              argument.remove(argument.size()-1);
            }
          }
          // mkdir
          if (command.equals("mkdir")) {
            Mkdir x = new Mkdir(command, argument);
            // check if the argument is valid
            if (x.isValidArgument(current) == true) {
              x.execute(root);
            }
            // ls
          }
          else if (command.equals("ls")) {
            Boolean r = false;
            if(argument.size() != 0)
            {
              // recursive list
              if(argument.get(0).equals("-r") | argument.get(0).equals("-R"))
              {
                r = true;
                argument.remove(0);
              }
            }
            Ls x = new Ls(command, argument);
            if (x.isValidArgument(current) == true) 
            {
              String output = x.execute(root, current, r);
              //redirection
              if(redirection == true){
                Redirection re = new Redirection(
                    output, arrow, pathDirectoryRedirection);
                re.execute(root);
              //otherwise
              }else{
                System.out.println(output);
              }
              // redirection
            }
            // cd, pushd
          } else if (command.equals("cd") | command.equals("pushd")) {
            //pushd
            if (command.equals("pushd")){
              stack.push(current);
            }
            //cd
            Cd x = new Cd(command, argument);
            if (x.isValidArgument(current) == true) {
              current = x.execute(root, current);
            }
            // pwd
          } else if (command.equals("pwd")) {
            Pwd x = new Pwd(command, argument);
            if (x.isValidArgument(current) == true) {
              String output = x.execute(current);
              if(redirection == true){
                Redirection re = new Redirection(
                    output, arrow, pathDirectoryRedirection);
                re.execute(root);
              }else{
                System.out.println(output);
              }
            }
            // cp
          } else if (command.equals("cp")) {
            Cp_Mv x = new Cp_Mv(command, argument);
            if (x.isValidArgument(current) == true) {
              x.executeCp(root);
            }
            // mv
          } else if (command.equals("mv")) {
            Cp_Mv x = new Cp_Mv(command, argument);
            if (x.isValidArgument(current) == true) {
              String s = x.executeMv(root, current);
              if(!s.equals("")){
                System.out.println(s);
              }
            }
            // man
          } else if (command.equals("man")) {
            Man man = new Man(command, argument);
            if (man.isValidArgument(current) == true) {
              String doc = man.printDoc(man.getArgument().get(0));
              if(redirection == true){
                Redirection re = new Redirection(
                    doc, arrow, pathDirectoryRedirection);
                re.execute(root);
              }else{
                System.out.println(doc);
              }
            } else {
              System.out.println("The argument is not valid");
            }
            // echo
          }else if (command.equals("echo")) {
            Echo x = new Echo(command, argument);
            if (x.isValidArgument(current) == true) {
              String output = x.execute();
              if(redirection == true){
                Redirection re = new Redirection(
                    output, arrow, pathDirectoryRedirection);
                re.execute(root);
              }else{
                System.out.println(output);
              }
            }
            //popd
          }else if (command.equals("popd")){
            if (stack.isEmpty()){
              System.out.println("Error, the stack is empty");
            }
            else{
              current = stack.pop();
            } 
          }else if(command.equals("get")){
            GetUrl g1= new GetUrl(command, argument);
            File tempFile;
            if (g1.ValidArgument1() == true){
              try {
                tempFile=g1.readUrl(argument);
                //System.out.println(argument);
                ArrayList<String> names=new ArrayList<String>();
                for(int i1=0;i1<current.getChildren().size();i1+=1){
                  names.add(current.getChildren().get(i1).getName());
                }
                if(names.contains(tempFile.getName())== true){
                  System.out.println("File already exists");
                }else{
                  current.addFile(g1.readUrl(argument));  
                }
              } catch (IOException e) {
                //e.printStackTrace();
                System.out.println("Invalid URL");
              }catch(IllegalArgumentException e){
                System.out.println("Invalid URL");
              }
            }else{
              System.out.println("Invalid Url. Try Again.");
            }
            //System.out.println(g1.inputLine1);
          //cat
          }else if(command.equals("cat")){
            Cat x= new Cat(command, argument);
            if(x.isValidArgument(current)==true){
              String output = x.execute(root);
              //redirection
              if(redirection == true){
                Redirection re = new Redirection(
                    output, arrow, pathDirectoryRedirection);
                re.execute(root);
              }else{
                  System.out.print(output);
              }
            }
          }else if(command.equals("rm")){
            Boolean confirm = true;
            if(argument.size() != 0)
            {
              // confirmation
              if(argument.get(0).equals("-f"))
              {
                confirm = false;
                argument.remove(0);
              }
            }
            Rm x = new Rm(command, argument);
            // check if the argument is valid
            if (x.isValidArgument(current) == true) {
              if(confirm == true){
                //add all possible paths to a list
                Boolean error = false;
                ArrayList<ArrayList<String>> lst = 
                    new ArrayList<ArrayList<String>>();
                for(int p=0; p<x.getListPathDirectory().size(); p++){
                  ArrayList<String> pathDirectory = 
                      x.getListPathDirectory().get(p); 
                  FileName f = x.findFile(root, current, pathDirectory);
                  if(f != null){
                    x.recursiveRm(f, lst, current);
                  }
                }
                x.setListPathDirectory(lst);
              }
              //redirection
              if(redirection==true){
                for(int m=0; m<x.getListPathDirectory().size(); m++){
                  ArrayList<String> pathDirectory = 
                      x.getListPathDirectory().get(m);
                  //recursive romove
                  if(confirm == true){
                    String s = null;
                    BufferedReader c =
                        new BufferedReader(new InputStreamReader(System.in));
                    String path = "";
                    for(int n=0; n<pathDirectory.size(); n++){
                      path = path + "/" + pathDirectory.get(n);
                    }
                    if(path.equals("")){
                      path = "/";
                    }
                    String output = "Do you want to delete " + path + "? ";
                    if(m == 0){
                      //save output
                      Redirection re = new Redirection(
                          output, arrow, pathDirectoryRedirection);
                      re.execute(root);
                    }else{
                      Redirection re = new Redirection(
                          output, ">>", pathDirectoryRedirection);
                      re.execute(root);
                    }
                    try {
                      s = c.readLine();
                    } catch (IOException e) {
                      e.printStackTrace();
                    }
                    Redirection re = new Redirection(
                        s, ">>", pathDirectoryRedirection);
                    re.execute(root);
                    if(s.equals("Yes")){
                      x.execute(root, current, pathDirectory);
                    }else if(s.equals("No")){
                      
                    }else{
                      System.out.println("Error: type 'Yes' or 'No'");
                    }
                  //otherwise
                  }else{
                    String s = x.execute(root, current, pathDirectory);
                    if(s != null){
                      System.out.println(s);
                    }
                  }
                }
              //no redirection
              }else{
                for(int m=0; m<x.getListPathDirectory().size(); m++){
                  ArrayList<String> pathDirectory = 
                      x.getListPathDirectory().get(m);
                  //need confirmation
                  if(confirm == true){
                    String s = null;
                    BufferedReader c =
                        new BufferedReader(new InputStreamReader(System.in));
                    String path = "";
                    for(int n=0; n<pathDirectory.size(); n++){
                      path = path + "/" + pathDirectory.get(n);
                    }
                    if(path.equals("")){
                      path = "/";
                    }
                    System.out.print("Do you want to delete " + path + "? ");
                    try {
                      s = c.readLine();
                    } catch (IOException e) {
                      e.printStackTrace();
                    }
                    if(s.equals("Yes")){
                      x.execute(root, current, pathDirectory);
                    }else if(s.equals("No")){
                      
                    }else{
                      System.out.println("Error: type 'Yes' or 'No'");
                    }
                  //otherwise
                  }else{
                    String s = x.execute(root, current, pathDirectory);
                    if(s != null){
                      System.out.println(s);
                    }
                  }
                }
              }
            }
          //grep
          }else if(command.equals("grep")){
            Boolean r = false;
            if(argument.size() != 0)
            {
              // recursive list
              if(argument.get(0).equals("-r") | argument.get(0).equals("-R"))
              {
                r = true;
                argument.remove(0);
              }
            }
            Grep x = new Grep(command, argument);
            if (x.isValidArgument(current) == true) 
            {
              String output = x.execute(root, current, r);
              //redirection
              if(redirection == true){
                Redirection re = new Redirection(
                    output, arrow, pathDirectoryRedirection);
                re.execute(root);
              //otherwise
              }else{
                System.out.println(output);
              }
            }
          }
          // other command
          else {
            System.out.println("The command is invalid, please try again.");
          }
        }
      }
    }
  }
  
}
