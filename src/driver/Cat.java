package driver;

import java.util.ArrayList;

/**
 * 
 * This class displays the content of a file
 * 
 */
public class Cat extends AbstractCommand {
  
  //List of strings of directories along the path
  private ArrayList<String> pathDirectory = new ArrayList<String>();

  /**
   * Creates a cat object.
   * 
   * 
   * @param command Command to be created.
   * @param lst List containing arguments.
   */
  public Cat(String command, ArrayList<String> lst){
    super(command, lst);
  }
  
  /**
   * 
   * Returns Directory Path
   * @return Directory Path
   * 
   */
  public ArrayList<String> getPathDirectory() {
    return pathDirectory;
  }

  /**
   * 
   * Sets DirectoryPath
   * @param pathDirectory List which has directory path
   * 
   */
  public void setPathDirectory(ArrayList<String> pathDirectory) {
    this.pathDirectory = pathDirectory;
  }

  /**
   * Checks if argument(s) are valid.
   * 
   * @param current working directory
   * @return if the argument is valid or not
   */
  public Boolean isValidArgument(Directory current){
    ArrayList<String> names=new ArrayList<String>();
    if(getArgument().size()==1){
      if(isValidPath(getArgument().get(0), pathDirectory, current)==true)
      {
        return true;
      }
    }
    System.out.println("The argument is not valid");
    return false;
  }
  
  /**
   * Return the contents of the file
   * 
   * 
   * @param root Root directory
   * @return the contend of the file
   */
  public String execute(Directory root){
    String s = "";
    if(pathDirectory.size()==0){
      System.out.println("Invalid path");
    }else{
      int x = 0;
      int m = 1;
      Directory r = root;
      // check if path exists
      while (x < pathDirectory.size()-1) {
        m = 0;
        int y = 0;
        int n = 0;
        while (y < r.getChildren().size()) {
          n = 0;
          if (pathDirectory.get(x).equals(r.getChildren().get(y).getName())
              & r.getChildren().get(y) instanceof Directory) {
            r = (Directory) r.getChildren().get(y);
            n = 1;
            break;
          }
          n = 0;
          y++;
        }
        if (n == 0) {
          m = 0;
          System.out.println("Path not found");
          break;
        }
        m = 1;
        x++;
      }
      // path found
      if (m == 1) {
        int i=0;
        boolean ff = false;
        while(i<r.getChildren().size()){
          if (pathDirectory.get(pathDirectory.size()-1).
              equals(r.getChildren().get(i).getName())
              & r.getChildren().get(i) instanceof File) {
            for(int j=0; j<((File) r.getChildren().get(i)).getContent().size(); 
                j++){
              s = s + ((File) r.getChildren().get(i)).
                  getContent().get(j) + "\n";
              ff = true;
            }
          }
          i++;
        }
        if(ff==false){
          System.out.println("File not found");
        }
      }
    }
    return s;
  }
  
}
