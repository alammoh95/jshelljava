package driver;

import java.util.ArrayList;

/**
 * 
 * This class is used to move files and directories
 * 
 */
public class Cp_Mv extends AbstractCommand {

  // List of strings of directories along the path
  private ArrayList<String> pathDirectory1 = new ArrayList<String>();
  private ArrayList<String> pathDirectory2 = new ArrayList<String>();

  /**
   * Creates a Cp_Mv object.
   * 
   * 
   * @param command Command to be created.
   * @param lst List containing arguments.
   */
  public Cp_Mv(String command, ArrayList<String> lst) {
    super(command, lst);
  }

  /**
   * Checks if argument(s) are valid.
   * 
   * @param current Current working directory
   * @return if the argument is valid or not
   */
  public Boolean isValidArgument(Directory current)
  // check if the form of the argument is valid
  {
    // two arguments
    if (getArgument().size() == 2) {
      if (isValidPath(getArgument().get(0), pathDirectory1, current) == true 
          & isValidPath(getArgument().get(1), pathDirectory2, 
              current) == true){
        return true;
      }
      System.out.println("The argument is not valid");
      return false;
    }
    // else
    System.out.println("The argument is not valid");
    return false;
  }


  /**
   * Returns list of directories along the path
   * 
   * 
   * @param None
   * @return the list of path directory of the original path
   */
  public ArrayList<String> getPathDirectory1() {
    return pathDirectory1;
  }

  /**
   * Returns list of directories along the path
   * 
   * 
   * @param None
   * @return the list of path directory of the destination path
   */
  public ArrayList<String> getPathDirectory2() {
    return pathDirectory2;
  }

  /**
   * Executes the copy command.
   * 
   * @param root Root directory.
   * @return None
   */
  public void executeCp(Directory root) {
    ArrayList<String> d1 = this.getPathDirectory1();
    ArrayList<String> d2 = this.getPathDirectory2();
    // original path cant be "/"
    if (d1.size() == 0) {
      System.out.println("Invalid move");
      // from a path to current
    }else{
      Directory r1 = root;
      int i1 = 0;
      int n1 = 1;
      while (i1 < d1.size()-1) {
        n1 = 0;
        int j = 0;
        int b = 0;
        while (j < r1.getChildren().size()) {
          b = 0;
          if (d1.get(i1).equals(r1.getChildren().get(j).getName())
              & r1.getChildren().get(j) instanceof Directory) {
            // find the original one
            r1 = (Directory) r1.getChildren().get(j);
            b = 1;
            break;
          }
          b = 0;
          j++;
        }
        if (b == 0) {
          System.out.println("Original path not found");
          n1 = 0;
          break;
        }
        n1 = 1;
        i1++;
      }
      Directory d = null;
      Boolean db = false;
      File f = null;
      Boolean fb = false;
      if(n1==1){
        int q=0;
        while(q < r1.getChildren().size()){
          if(d1.get(d1.size()-1).equals(r1.getChildren().get(q).getName()) & 
              r1.getChildren().get(q) instanceof File){
            f = (File) r1.getChildren().get(q);
            fb = true;
            break;
          }else if(d1.get(d1.size()-1).equals(r1.getChildren().
              get(q).getName()) & 
              r1.getChildren().get(q) instanceof Directory){
            d = (Directory) r1.getChildren().get(q);
            db = true;
            break;
          }
          q++;
        }
        if(q==r1.getChildren().size()){
          System.out.println("Original path not found");
          n1 = 0;
        }
      }
      Directory r2 = root;
      int i2 = 0;
      int n2 = 1;
      while (i2 < d2.size()) {
        n2 = 0;
        int j = 0;
        int b = 0;
        while (j < r2.getChildren().size()) {
          b = 0;
          if (d2.get(i2).equals(r2.getChildren().get(j).getName())
              & r2.getChildren().get(j) instanceof Directory) {
            // find the destination
            r2 = (Directory) r2.getChildren().get(j);
            b = 1;
            break;
          }
          b = 0;
          j++;
        }
        if (b == 0) {
          System.out.println("Destination path not found");
          n2 = 0;
          break;
        }
        n2 = 1;
        i2++;
      }
      if (n1 == 1 & n2 == 1) {
        // copy the original one
        if(db==true){
          Directory dd = d.copy();
          r2.addDirectory(dd);
        }else if(fb==true){
          File ff = f.copy();
          r2.addFile(ff);
        }
        root = r2;
        while (!root.getParentDirectory().getName().equals("")) {
          root = root.getParentDirectory();
        }
      }
    }
  }

  /**
   * Executes the move command.
   * 
   * @param root Root directory.
   * @param current Current directory object.
   * @return None
   */
  public String executeMv(Directory root, Directory current) {
    ArrayList<String> d1 = this.getPathDirectory1();
    ArrayList<String> d2 = this.getPathDirectory2();
    // original path cant be "/"
    if (d1.size() == 0) {
      return "Invalid move";
    }else{
      if (d2.size() >= d1.size()) {
        int i = 0;
        while (i < d1.size()) {
          if (!d1.get(i).equals(d2.get(i))) {
            break;
          }
          i++;
        }
        // move a directory in itself is not valid
        if (i == d1.size()) {
          return "Invalid move";
        }
      }
      ArrayList<String> lst = new ArrayList<String>();
      Pwd pwd = new Pwd("pwd", lst);
      String path = pwd.execute(current);
      ArrayList<String> pathDirectory3 = new ArrayList<String>();
      Boolean a = isValidPath(path, pathDirectory3, current);
      if (pathDirectory3.size() >= d1.size()) {
        int i = 0;
        while (i < d1.size()) {
          if (!d1.get(i).equals(pathDirectory3.get(i))) {
            break;
          }
          i++;
        }
        // move a directory that is currently working is not valid
        if (i == d1.size()) {
          return "Invalid move";
        }
      }
      Directory r1 = root;
      int i1 = 0;
      int n1 = 1;
      while (i1 < d1.size()-1) {
        n1 = 0;
        int j = 0;
        int b = 0;
        while (j < r1.getChildren().size()) {
          b = 0;
          if (d1.get(i1).equals(r1.getChildren().get(j).getName())
              & r1.getChildren().get(j) instanceof Directory) {
            // find the original path
            r1 = (Directory) r1.getChildren().get(j);
            b = 1;
            break;
          }
          b = 0;
          j++;
        }
        if (b == 0) {
          n1 = 0;
          return "Original path not found";
        }
        n1 = 1;
        i1++;
      }
      Directory d = null;
      Boolean db = false;
      File f = null;
      Boolean fb = false;
      if(n1==1){
        int q=0;
        while(q < r1.getChildren().size()){
          if(d1.get(d1.size()-1).equals(r1.getChildren().get(q).getName()) & 
              r1.getChildren().get(q) instanceof File){
            f = (File) r1.getChildren().get(q);
            fb = true;
            break;
          }else if(d1.get(d1.size()-1).equals(r1.getChildren().
              get(q).getName()) & 
              r1.getChildren().get(q) instanceof Directory){
            d = (Directory) r1.getChildren().get(q);
            db = true;
            break;
          }
          q++;
        }
        if(q==r1.getChildren().size()){
          return "Original path not found";
        }
      }
      Directory r2 = root;
      int i2 = 0;
      int n2 = 1;
      while (i2 < d2.size()) {
        n2 = 0;
        int j = 0;
        int b = 0;
        while (j < r2.getChildren().size()) {
          b = 0;
          if (d2.get(i2).equals(r2.getChildren().get(j).getName())
              & r2.getChildren().get(j) instanceof Directory) {
            // find the destination path
            r2 = (Directory) r2.getChildren().get(j);
            b = 1;
            break;
          }
          b = 0;
          j++;
        }
        if (b == 0) {
          return "Destination path not found";
        }
        n2 = 1;
        i2++;
      }
      if (n1 == 1 & n2 == 1) {
        //copy the original one
        int i3=0;
        int i4=0;
        if(db==true){
          Directory dd = d.copy();
          while(i3<r2.getChildren().size()){
            // directory already exists
            if((r2.getChildren().get(i3)).sameName(d)){
              return "Directory with the same name already exists";
            }
            i3++;
          }
          if(i3==r2.getChildren().size()){
            r2.addDirectory(dd);
            root = r2;
            while (!root.getParentDirectory().getName().equals("")) {
              root = root.getParentDirectory();
            }
            int x = 0;
            while (x < d1.size() - 1) {
              int y = 0;
              while (y < root.getChildren().size()) {
                if (d1.get(x).equals(root.getChildren().get(y).getName())
                    & root.getChildren().get(y) instanceof Directory) {
                  root = (Directory) root.getChildren().get(y);
                  break;
                }
                y++;
              }
              x++;
            }
            root.deleteDirectoryOrFile(d1.get(d1.size() - 1));
            while (!root.getParentDirectory().getName().equals("")) {
              root = root.getParentDirectory();
            }
          }
        }else if(fb==true){
          File ff = f.copy();
          while(i4<r2.getChildren().size()){
            // directory already exists
            if((r2.getChildren().get(i4)).sameName(d)){
              return "Directory with the same name already exists";
            }
            i4++;
          }
          if(i4==r2.getChildren().size()){
            r2.addFile(ff);
            root = r2;
            while (!root.getParentDirectory().getName().equals("")) {
              root = root.getParentDirectory();
            }
            int x = 0;
            while (x < d1.size() - 1) {
              int y = 0;
              while (y < root.getChildren().size()) {
                if (d1.get(x).equals(root.getChildren().get(y).getName())
                    & root.getChildren().get(y) instanceof Directory) {
                  root = (Directory) root.getChildren().get(y);
                  break;
                }
                y++;
              }
              x++;
            }
            root.deleteDirectoryOrFile(d1.get(d1.size() - 1));
            while (!root.getParentDirectory().getName().equals("")) {
              root = root.getParentDirectory();
            }
          }
        }
      }
    }
    return "";
  }

}
