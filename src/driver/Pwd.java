package driver;

import java.util.ArrayList;

/**
 * 
 * This class is used to print the working directory
 * 
 */
public class Pwd extends AbstractCommand {

  /**
   * Creates a Pwd object.
   * 
   * 
   * @param command Command to be created.
   * @param lst List containing arguments.
   */
  public Pwd(String command, ArrayList<String> lst) {
    super(command, lst);
  }

  /**
   * Checks if argument(s) are valid.
   * 
   * @param current Current working directory
   * @return if the argument is valid
   */
  public Boolean isValidArgument(Directory current)
  // check if the form of the argument is valid
  {
    // no argument
    if (getArgument().size() == 0) {
      return true;
    }
    System.out.println("The argument is not valid");
    return false;
  }
  
  /**
   * Executes the print directory path command.
   * 
   * @param current Current directory object.
   */
  public String execute(Directory current) {
    String s = "";
    // in root directory
    if (current.getParentDirectory().getName().equals("")) {
      return "/";
    } else {
      while (!current.getParentDirectory().getName().equals("")) {
        s = "/" + current.getName() + s;
        current = current.getParentDirectory();
      }
      return s;
    }
  }

}
