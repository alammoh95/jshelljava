package driver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class GetUrl extends AbstractCommand {

  public String inputLine1="";
  /**
   * 
   * Creates a GetUrl object
   * @param command The Command get
   * @param lst List of arguments
   */
  public GetUrl(String command, ArrayList<String> lst) {
    super(command, lst);
  }
  
  /**
   * 
   * Checks to see if Arguments are valid
   * @return Boolean
   * 
   */
  public Boolean ValidArgument1(){
    ArrayList<String> argument=this.getArgument();
    int fla1=0;
    if(argument.size()!=1){
      if(argument.size()!=3){
        //if its not 1 or 3 it is wrong
        return false;
      }
    }
    fla1=1;
    //If url does not have one of these extensions it is invalid.
    if(argument.get(0).contains(".txt")==false){
      if(argument.get(0).contains(".pdf")==false){
        if(argument.get(0).contains(".html")==false){
          return false;
        }
      }
    }
    //If URL does not have http or www it is invalid
    if(argument.get(0).contains("http")==false){
      if(argument.get(0).contains("www")==false){
        return false;
      }
    }
    int flag=0;
    ArrayList<Integer> flag1=new ArrayList<Integer>();
    for(int i=0; i<argument.get(0).length();i+=1){
      if(String.valueOf(argument.get(0).charAt(i))=="."){
        //there cannot be a / after .
        if(String.valueOf(argument.get(0).charAt(i+1))=="/"){
          return false;
        }
      }
      if(String.valueOf(argument.get(0).charAt(i))=="/"){
        if(String.valueOf(argument.get(0).charAt(i-1))=="/"){
          //if before it is slash then after there should be no slash
          if(String.valueOf(argument.get(0).charAt(i+1))=="/"){
            return false;
          }
        }else if(String.valueOf(argument.get(0).charAt(i-1))!="/"){
          //if this is first bracket then after it should not be /
          //unless before it is http:
          if(String.valueOf(argument.get(0).charAt(i-1))!=":"){
            if(String.valueOf(argument.get(0).charAt(i+1))=="/"){
              return false;
            }
          }
        }
      }
    }
    if(flag==1){
      return false;
    }
    if(flag1.contains(1)){
      return false;
    }
    //if everything is right in url then return true
    return true;
  }
  /**
   * 
   * Downloads data from URL.
   * @param lst List of arguments.
   * @return File with contents from URL.
   * 
   */
  public File readUrl(ArrayList<String>lst) throws IOException{
    //gets the website
    String url= lst.get(0);
    //Creates a Url object 
    URL myURL = new URL(url);
    //starts connection
    URLConnection myURLConnection = myURL.openConnection();
    myURLConnection.connect();
    URLConnection yc = myURL.openConnection();
    //Starts BufferedReader
    BufferedReader in = new BufferedReader(new InputStreamReader(
                                yc.getInputStream()));
    String inputLine;
    //keeps reading until end of file is reached
    while ((inputLine = in.readLine()) != null) 
        inputLine1=inputLine1+ "\n" + inputLine;
    //closes the file
    in.close();
    String s= inputLine1;
    int i1=0, i2=0;
    //determines file name based on file extension location
    if( lst.get(0).endsWith(".txt")){
      i1=lst.get(0).indexOf(".txt", 0);
      i2= lst.get(0).lastIndexOf("/");      
    }else if(lst.get(0).endsWith(".pdf")){
      i1=lst.get(0).indexOf(".pdf", 0);
      i2= lst.get(0).lastIndexOf("/");
    }else if(lst.get(0).endsWith(".html")){
      i1=lst.get(0).indexOf(".html", 0);
      i2= lst.get(0).lastIndexOf("/");
    }
    //Stores the name 
    String name=lst.get(0).substring(i2+1, i1);
    //Creates the file with the contents and fileName
    File newFile= new File(name);
    newFile.addContent(s);
    return newFile;
  }

  @Override
  public Boolean isValidArgument(Directory current) {
    // TODO Auto-generated method stub
    return null;
  }
}
