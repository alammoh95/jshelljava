package driver;

import java.util.ArrayList;

/**
 * 
 * This class is inherited by File and Directory and holds the name
 * 
 */
public class FileName {

  // name of file or directory
  private String name;
  // parent directory
  private Directory parentDirectory;
  
  /**
   * Creates a FileName object.
   * 
   * 
   * @param s Name of File.
   */
  public FileName(String s) {
    name = s;
    
  }
  
  /**
   * Rename a file or a directory
   * 
   * 
   * @param name A new name
   */
  public void rename(String name){
    this.name = name;
  }
  
  /**
   * Return iff two directories or files have the same name
   * 
   * 
   * @param a A directory or a file
   * @return if they have the same name or not
   */
  public Boolean sameName(FileName f) {
    return this.name.equals(f.name);
  }

  /**
   * Get the name of a file or a directory
   * 
   * 
   * @param None
   * @return The name
   */
  public String getName(){
    return name;
  }
  
  /**
   * Get the parent directory
   * 
   * 
   * @param None
   * @return The parent directory
   */
  public Directory getParentDirectory(){
    return parentDirectory;
  }
  
  /**
   * Set the parent directory to the Directory d
   * 
   * 
   * @param d A directory
   * @return The parent directory
   */
  public void setParentDirectory(Directory d){
    parentDirectory = d;
  }
  
}
