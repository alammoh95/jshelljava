package driver;

import java.util.ArrayList;

/**
 * 
 * This class contains the manual of each command
 * 
 */
public class Man extends AbstractCommand {
  
  private ArrayList<String> valid = new ArrayList<String>();

  /**
   * Creates a Man object.
   * 
   * 
   * @param s Command to be created.
   * @param lst List containing arguments.
   */
  public Man(String s, ArrayList<String> lst) {
    super(s, lst);
    valid.add("cp");
    valid.add("mv");
    valid.add("ls");
    valid.add("mkdir");
    valid.add("cd");
    valid.add("pwd");
    valid.add("exit");
    valid.add("man");
    valid.add("get");
    valid.add("pushd");
    valid.add("popd");
    valid.add("cat");
    valid.add("echo");
    valid.add("rm");
    valid.add("grep");
    // TODO Auto-generated constructor stub
  }

  public Boolean isValidArgument(Directory current) {
    if (valid.contains(getArgument().get(0))) 
    {
      if(getArgument().size() == 1)
      {
        return true;
      }
    }
    return false;
  }
  
  public String printDoc(String command) {
    String documentation = "";
    if (command.equals("cd")) {
      documentation =
          "cd DIR -- Change directory to given directory if it exists.";
      documentation =
          documentation + "\n          Takes a directory name as an ";
      documentation =
          documentation + "argument. If .. is the argument it goes ";
      documentation = documentation + "to root directory.";
    } else if (command.equals("man")) {
      documentation = "man COMMAND -- Print the documentation of command.";

    } else if (command.equals("pwd")) {
      documentation = "pwd -- Print the working directory.";
    } else if (command.equals("ls")) {
      documentation = "ls [-R] PATH -- Lists all files and directories. If \n"
          + "                PATH is provided it lists for that path.\n "
          + "               If PATH is a file, prints PATH. \n"
          + "                If -R is present, \n"
          + "                recursively lists all subdirectories. ";
      documentation = documentation + " directory.";
    } else if (command.equals("mkdir")) {
      documentation = "mkdir DIR -- Create directories, each of which may be ";
      documentation =
          documentation + "relative to the current directory or may";
      documentation = documentation + " be a full path.";
    } else if (command.equals("exit")) {
      documentation = "exit -- Quit the program.";
    } else if (command.equals("mv")) {
      documentation =
          "mv OLDPATH NEWPATH -- Move item OLDPATH to NEWPATH. \n"
              + "                      Both OLDPATH and NEWPATH may \n"
              + "                      be relative to the current directory or"
              + " may be full paths. If \n"
              + "                      NEWPATH is a directory, move the item "
              + " into  the directory.";
    } else if (command.equals("cp")) {
      documentation =
          "cp OLDPATH NEWPATH -- Copy item OLDPATH to NEWPATH. \n"
              + "                      Both OLDPATH and NEWPATH may \n"
              + "                      be relative to the current directory or "
              + "may be full paths. If \n"
              + "                      OLDPATH is a directory, recursively "
              + "copy the items  into the \n"
              + "                      directory.";
    } else if(command.equals("get")){
      documentation= "get URL -- URL is a web address. Retrieve the file at" +
          "that URL and add it to the \n"
          + "           current working directory.";
    }else if(command.equals("popd")){
    documentation= "popd -- Removes the top entry from the directory stack, \n"
        + "        and cd to the new top directory. The popd command removes "
        + "the \n"
        + "        top most directory on the non-empty stack and makes it \n"
        + "        the current working directory.   ";
  }else if(command.equals("pushd")){
    documentation= "pushd DIR -- Saves the current working directory on the\n"
        + "             Stack and then changes to the current working \n"
        + "             directory to DIR.";
  }else if(command.equals("cat")){
    documentation = "cat FILE -- Display the contents of FILE in shell.";
  }else if (command.equals("echo")){
    documentation = "echo String -- Print String";
  }else if(command.equals("rm")){
    documentation = "rm [-f] PATH -- Removes PATH with confirmation. If PATH \n"
        + "                is a directory, recursively removes all files \n"
        + "                and subdirectories, prompting confirmation \n"
        + "                for each one. If -f is present it does not \n"
        + "                ask confirmation.";
  }
    else {
      documentation = "Invalid command! Try again!";
    }
    return documentation;

  }


}
