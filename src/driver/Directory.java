package driver;

import java.util.ArrayList;

/**
 * 
 * This class is used to hold files and directories It represents a Directory
 * object
 */
public class Directory extends FileName {

  // directories or files in the directory
  private ArrayList<FileName> children = new ArrayList<FileName>();

  /**
   * Creates a Directory object.
   * 
   * 
   * @param s Directory name.
   */
  public Directory(String s) {
    super(s);
  }

  /**
   * Add directory d to the current one
   * 
   * 
   * @param d A directory.
   */
  public void addDirectory(Directory d){
    int i=0;
    while(i<children.size()){
      // directory already exists
      if((children.get(i)).sameName(d)){
        System.out.println("Directory with the same name already exists");
        break;
      }
      i++;
    }
    if(i==children.size()){
      children.add(d);
      d.setParentDirectory(this);
    }
  }
  
  /**
   * Add file d to the current directory
   * 
   * 
   * @param d A file.
   */
  
  public void addFile(File d){
    int i=0;
    while(i<children.size()){
      // directory already exists
      if((children.get(i)).sameName(d)){
        System.out.println("File with the same name already exists");
        break;
      }
      i++;
    }
    if(i==children.size()){
      children.add(d);
      d.setParentDirectory(this);
    }
  }
  
  
  
  /**
   * Remove a directory or a file with name s from the current directory
   * 
   * 
   * @param s The directory or file name.
   */
  public void deleteDirectoryOrFile(String s){
    int i=0;
    while(i<this.children.size()){
      //find d
      if ((children.get(i)).getName().equals(s)){
        // remove the orignal one
        children.remove(i);
        break;
      }
      i++;
    }
  }

  /**
   * Copy the current directory
   * 
   * 
   * @param none
   * @return the copy of current directory
   */
  public Directory copy() {
    if (children.size() == 0) {
      Directory a = new Directory(getName());
      return a;
    } else {
      Directory a = new Directory(getName());
      for (int i = 0; i < children.size(); i++) {
        if(children.get(i) instanceof Directory){
          a.addDirectory(((Directory) children.get(i)).copy());
        }else{
          a.addFile(((File) children.get(i)).copy());
        }
      }
      return a;
    }
  }
  
  /**
   * Get the parent directory
   * 
   * 
   * @param None
   * @return The parent directory
   */
  public ArrayList<FileName> getChildren(){
    return children;
  }
}
