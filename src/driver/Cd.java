package driver;

import java.util.ArrayList;

/**
 * 
 * This class is used to change directories
 * 
 */
public class Cd extends AbstractCommand {

  // List of strings of directories along the path
  private ArrayList<String> pathDirectory = new ArrayList<String>();

  /**
   * Creates a ChangeDirectory object.
   * 
   * 
   * @param command Command to be created.
   * @param lst List containing arguments.
   */
  public Cd(String command, ArrayList<String> lst) {
    super(command, lst);
  }

  /**
   * Checks if argument(s) are valid.
   * 
   * @param current working directory
   * @return if the argument is valid or not
   */
  public Boolean isValidArgument(Directory current)
  // check if the form of the argument is valid
  {
    // one argument
    if (getArgument().size() == 1) {
      String s = getArgument().get(0);
      if (isValidPath(s, pathDirectory, current) == false) {
        System.out.println("The argument is not valid");
        return false;
      }
      return true;
    }
    // no argument
    if (getArgument().size() == 0) {
      return true;
    }
    // two or more argument
    System.out.println("The argument is not valid");
    return false;
  }

  /**
   * Return list of directories along the path
   * 
   * 
   * @param None
   */
  public ArrayList<String> getPathDirectory() {
    return pathDirectory;
  }

  /**
   * Changes current directory to new directory.
   * 
   * 
   * @param root Root of directory.
   * @param current Current directory.
   * @return the new current directory
   */
  public Directory execute(Directory root, Directory current) {
    // with path
    if (getArgument().size() == 1) {
      // path with /
      if (pathDirectory.size()==0) {
        current = root;      
      // cd in a directory with path
      }else{
        Directory d = root;
        ArrayList<String> list = this.getPathDirectory();
        int i = 0;
        while (i < list.size()) {
          int j = 0;
          int b = 0;
          while (j < d.getChildren().size()) {
            b = 0;
            // check the path
            if (list.get(i).equals(d.getChildren().get(j).getName())
                & d.getChildren().get(j) instanceof Directory) {
              d = (Directory) d.getChildren().get(j);
              b = 1;
              break;
            }
            b = 0;
            j++;
          }
          // path not found
          if (b == 0) {
            System.out.println("Path not found");
            d = current;
            break;
          }
          i++;
        }
        current = d;
      }
      // return to the root directory
    } else if (getArgument().size() == 0) {
      current = root;
    }else{
      System.out.println("The argument is not valid");
    }
    return current;
  }
}
