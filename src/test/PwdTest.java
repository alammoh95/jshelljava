package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

import driver.Directory;
import driver.Pwd;

public class PwdTest {

  public ArrayList<String> a1 = new ArrayList<String>();

  @Test
  public void testPwd() {
    Pwd pwd = new Pwd("pwd", a1);
    assertEquals("It should be: ", "pwd", pwd.getCommand());
    assertEquals("It should be: ", 0, pwd.getArgument().size());
  }

  @Test
  public void testIsValidArgument() {
    a1.add("hello");
    Pwd pwd = new Pwd("pwd", a1);
    assertFalse(pwd.isValidArgument(null));
    a1.clear();
    pwd = new Pwd("pwd", a1);
    assertTrue(pwd.isValidArgument(null));
  }

  @Test
  public void testExecute() {
    Directory root = new Directory("Ha");
    ArrayList<String> a2 = new ArrayList<String>();
    Pwd pwd1 = new Pwd("pwd", a2);
    // a2.add("");
    Directory current = new Directory("");
    root.setParentDirectory(current);
    assertEquals("It should be: ", "/", pwd1.execute(root));
  }

}
