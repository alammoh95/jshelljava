package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;

import org.junit.Test;

import driver.AbstractCommand;
import driver.Directory;
import driver.Man;

public class AbstractCommandTest {


  public ArrayList<String> a1 = new ArrayList<String>();
  public AbstractCommand ab = new Man("someCommand", a1);
  public ArrayList<String> dList = new ArrayList<String>();
  public Directory current = new Directory("/");
  public Directory root = current;

  @Test
  public void testAbstractCommand() {
    a1.add("someArgument");
    assertEquals("Command should be: ", "someCommand", ab.getCommand());
    assertEquals("Argument should be: ", "someArgument", 
        ab.getArgument().get(0));
  }
  
  @Test
  public void testAbsolutePath() {
    Directory d = new Directory("");
    root.setParentDirectory(d);
    assertEquals("/a", ab.absolutePath("a", current));
  }

  @Test
  public void testIsValidPath() {

    assertTrue(ab.isValidPath("/a/b", dList, current));
  }

  @Test
  public void testValidName() {
    assertFalse(ab.isValidName("*$#@!^&"));
  }

}
