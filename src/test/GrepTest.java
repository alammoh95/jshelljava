package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Before;
import org.junit.Test;

import driver.File;
import driver.FileName;
import driver.Grep;
import driver.Directory;

public class GrepTest {

  public ArrayList<String> arg = new ArrayList<String>();
  public Directory current = new Directory("/");
  public Directory root = current;
  public Directory parent = new Directory("");
  public Directory a = new Directory("a");
  public File atxt = new File("a.txt");
  public File btxt = new File("b.txt");
  
  @Before
  public void setUp(){
    root.setParentDirectory(parent);
    atxt.setContent("ABC");
    btxt.setContent("BCD");
    root.addFile(atxt);
    a.addFile(btxt);
    root.addDirectory(a);
    arg.add("[A-B]");
    arg.add("/");
  }

  @Test
  public void testGrep() {
    Grep x = new Grep("grep", arg);
    assertEquals("It should be: ", "grep", x.getCommand());
    assertEquals("It should be: ", "[A-B]", x.getArgument().get(0));
    assertEquals("It should be: ", "/", x.getArgument().get(1));
  }

  @Test
  public void testValidArgument() {
    Grep x = new Grep("grep", arg);
    assertTrue(x.isValidArgument(current));
  }

  @Test
  public void testGetListPathDirectory(){
    Grep x = new Grep("grep", arg);
    x.isValidArgument(current);
    assertEquals(1, x.getListPathDirectory().size());
  }
  
  @Test
  public void testGetAllContent(){
    Grep x = new Grep("grep", arg);
    x.isValidArgument(current);
    assertEquals(0, x.getAllContent().size());
  }
  
  @Test
  public void testRecursiveAddFileContent(){
    Grep x = new Grep("grep", arg);
    x.isValidArgument(current);
    x.recursiveAddFileContent(root);
    assertEquals("/a.txt: ABC", x.getAllContent().get(0));
    assertEquals("/a/b.txt: BCD", x.getAllContent().get(1));
  }
  
  @Test
  public void testAddFileContent(){
    Grep x = new Grep("grep", arg);
    x.isValidArgument(current);
    x.addFileContent(atxt);
    assertEquals("ABC", x.getAllContent().get(0));
  }
  
  @Test
  public void testFindRegex(){
    Grep x = new Grep("grep", arg);
    x.isValidArgument(current);
    x.addFileContent(atxt);
    assertEquals("ABC\n", x.findRegex(false));
  }
  
  @Test
  public void testExecute() {
    Grep x = new Grep("grep", arg);
    x.isValidArgument(current);
    assertEquals("It should be: ", "ABC\n", x.execute(root, current, false));
  }

}
