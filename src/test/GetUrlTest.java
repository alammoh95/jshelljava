package test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import driver.Directory;
import driver.Echo;
import driver.GetUrl;

public class GetUrlTest {
  public ArrayList<String> myList;
  public GetUrl g;
  @Before
  public void setUp(){
    myList = new ArrayList<String>();
    ArrayList<String> lst = new ArrayList<String>();
    g = new GetUrl("get", lst);
  }

  @Test
  public void testreadUrl() throws IOException {
    myList.clear();
    myList.add("http://www.cs.cmu.edu/~spok/grimmtmp/073.txt");
    assertEquals("File Name should be: ", "073", g.readUrl(myList).getName());
    myList.clear();
    myList.add("https://mcs.utm.utoronto.ca/~zingarod/236/lecture2/class.txt");
    assertEquals("File Name should be: ", "class", g.readUrl(myList).getName());
    myList.clear();
    myList.add("https://mcs.utm.utoronto.ca/~zingarod/236/lecture2/lecture2.pdf");
    assertEquals("File Name should be: ", "lecture2", g.readUrl(myList).getName());
  }
  
  @Test
  public void testIsValidArgument(){
    Directory root=new Directory("/");
    Directory current=root;
    assertNull(g.isValidArgument(current));
    assertNull(g.isValidArgument(current));
    assertNull(g.isValidArgument(current));
    
  }

}

