package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Before;
import org.junit.Test;

import driver.Rm;
import driver.Directory;

public class RmTest {

  public ArrayList<String> arg = new ArrayList<String>();
  public Directory current = new Directory("/");
  public Directory root = current;
  public Directory parent = new Directory("");
  public Directory a = new Directory("a");
  public Directory b = new Directory("b");
  
  @Before
  public void setUp(){
    root.setParentDirectory(parent);
    a.addDirectory(b);
    root.addDirectory(a);
    arg.add("a");
  }

  @Test
  public void testRm() {
    Rm x = new Rm("rm", arg);
    assertEquals("It should be: ", "rm", x.getCommand());
    assertEquals("It should be: ", "a", x.getArgument().get(0));
  }

  @Test
  public void testIsValidArgument() {
    Rm x = new Rm("rm", arg);
    assertTrue(x.isValidArgument(current));
  }

  @Test
  public void testGetListPathDirectory(){
    Rm x = new Rm("rm", arg);;
    x.isValidArgument(current);
    assertEquals("a", x.getListPathDirectory().get(0).get(0));
  }
  
  @Test
  public void testSetListPathDirectory(){
    Rm x = new Rm("rm", arg);
    ArrayList<ArrayList<String>> lst = new ArrayList<ArrayList<String>>();
    x.setListPathDirectory(lst);
    assertEquals(lst, x.getListPathDirectory());
  }
  
  @Test
  public void testFindFile(){
    Rm x = new Rm("rm", arg);
    x.isValidArgument(current);
    ArrayList<String> lst = new ArrayList<String>();
    lst.add("a");
    assertEquals("a", x.findFile(root, current, lst).getName());
  }
  
  @Test
  public void testRecursiveRm(){
    Rm x = new Rm("rm", arg);
    x.isValidArgument(current);
    ArrayList<ArrayList<String>> lst = new ArrayList<ArrayList<String>>();
    x.recursiveRm(a, lst, current);
    ArrayList<String> a = new ArrayList<String>();
    ArrayList<String> b = new ArrayList<String>();
    ArrayList<ArrayList<String>> c = new ArrayList<ArrayList<String>>();
    a.add("a");
    b.add("a");
    b.add("b");
    c.add(a);
    c.add(b);
    assertEquals(c, lst);
  }
  
  @Test
  public void testExecute() {
    Rm x = new Rm("rm", arg);
    x.isValidArgument(current);
    ArrayList<String> a = new ArrayList<String>();
    a.add("a");
    x.execute(root, current, a);
    assertEquals(0, root.getChildren().size());
  }

}
