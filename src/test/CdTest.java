package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

import driver.Cat;
import driver.Cd;
import driver.Directory;

public class CdTest {

  public ArrayList<String> args1 = new ArrayList<String>();
  public Directory current = new Directory("/");
  public Directory root = current;

  @Test
  public void testCd() {
    args1.add("a");
    Cd cd = new Cd("cd", args1);
    assertEquals("It should be: ", "cd", cd.getCommand());
    assertEquals("It should be: ", "a", cd.getArgument().get(0));
  }

  @Test
  public void testValidArgument() {
    args1.clear();
    args1.add("/Folder1");
    Cd cd = new Cd("cd", args1);
    assertTrue(cd.isValidArgument(current));
  }
  
  @Test
  public void testGetPathDirectory(){
    ArrayList<String >lst = new ArrayList<String>();    
    lst.add("/a");
    Cd cd=new Cd("cat", lst);
    cd.isValidArgument(current);
    assertEquals("a", cd.getPathDirectory().get(0));
  }

  @Test
  public void testExecute() {
    args1.clear();
    Cd cd = new Cd("cd", args1);
    current.setParentDirectory(root);
    assertEquals("It should be: ", "/", cd.execute(root, current).getName());
  }

}
