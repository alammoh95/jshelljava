package test;

import static org.junit.Assert.*;

import org.junit.Test;

import driver.File;

public class FileTest {
  
  public File file=new File("MyFile");
  
  @Test
  public void testFile() {
    assertEquals("It should be: ", "MyFile", file.getName());
  }

  @Test
  public void testGetContent(){
    file.setContent("New");
    assertEquals("It should be: ","New", file.getContent().get(0));
  }
  
  @Test
  public void testSetContent(){
    file.setContent("New1");
    file.setContent("New2");
    assertEquals("It should be: ","New2", file.getContent().get(0));
  }
  
  @Test
  public void testAddContent(){
    file.setContent("New1");
    file.addContent("New2");
    assertEquals("It should be: ","New1", file.getContent().get(0));
  }
  
  @Test
  public void testCopy(){
    File file2 = file.copy();
    assertEquals("It should be: ", file2.getName(), file.getName());
    //assertEquals("It should be: ", file2.content, file.content);
  }

}
