package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import driver.Cat;
import driver.Directory;
import driver.File;

public class DirectoryTest {

  @Test
  public void testDirectory() {
    Directory temp = new Directory("Test");
    assertEquals("It should be: ", "Test", temp.getName());
  }

  @Test
  public void testAddDirectory() {
    Directory temp = new Directory("Test");
    Directory child = new Directory("Child");
    temp.addDirectory(child);
    assertEquals("It should be: ", temp, child.getParentDirectory());

  }

  public void testDeleteDirectory() {
    Directory temp=new Directory("Test");
    Directory child=new Directory("Child");
    temp.addDirectory(child);
    temp.deleteDirectoryOrFile("Child");
    assertEquals("It should be: ", 0, temp.getChildren().size());
    
  }
  
  @Test
  public void testAddFile() {
    Directory temp=new Directory("Test");
    File child=new File("Child");
    temp.addFile(child);
    assertEquals("It should be: ", temp.getChildren().get(0), child);
    
  }
  
  public void testDeleteFile() {
    Directory temp=new Directory("Test");
    File child=new File("Child");
    temp.addFile(child);
    temp.deleteDirectoryOrFile("Child");
    assertEquals("It should be: ", temp.getChildren().size(), 0);
    
  }

  @Test
  public void testCopy() {
    Directory temp = new Directory("Test");
    assertEquals("It should be: ", temp.getName(), temp.copy().getName());
  }
  
  @Test
  public void testGetChildren(){
    Directory a = new Directory("a");
    Directory b = new Directory("b");
    a.addDirectory(b);
    assertEquals("b", a.getChildren().get(0).getName());
  }
  
}
