package test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import driver.Cat;
import driver.Directory;
import driver.File;
import driver.GetUrl;

public class CatTest {

  public Cat c;
  
  @Before
  public void setUp(){
    ArrayList<String >lst = new ArrayList<String>();    
    lst.add("/a");
    c=new Cat("cat", lst);
  }
  
  @Test
  public void testIsValidArgument() {
    Directory current = new Directory("/");
    assertTrue(c.isValidArgument(current));
  }
  
  @Test
  public void testGetPathDirectory(){
    Directory current = new Directory("/");
    c.isValidArgument(current);
    assertEquals("a", c.getPathDirectory().get(0));
  }
  
  @Test
  public void testExecute() throws IOException {
    ArrayList<String >ls1 = new ArrayList<String>();
    ls1.add("http://www.cs.cmu.edu/~spok/grimmtmp/073.txt");
    GetUrl g= new GetUrl("get",ls1);
    Directory root= new Directory("/");
    Directory current=root;
    root.getChildren().add(g.readUrl(ls1));
    File myFile=new File("test");
    myFile.setContent("");
    ArrayList<String> l1= new ArrayList<String>();
    l1.add("073");
    c=new Cat("cat", l1);
    c.setPathDirectory(l1);
    assertTrue(c.execute(root).contains("There"));
  }

}
