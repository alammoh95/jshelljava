package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import driver.Echo;
import driver.Directory;

public class EchoTest {

  public ArrayList<String> args1 = new ArrayList<String>();
  public Directory current = new Directory("/");
  public Directory root = current;
  public Directory parent = new Directory("");
  
  @Before
  public void setUp(){
    root.setParentDirectory(parent);
    args1.add("\"a\"");
  }

  @Test
  public void testEcho() {
    Echo echo = new Echo("echo", args1);
    assertEquals("It should be: ", "echo", echo.getCommand());
    assertEquals("It should be: ", "\"a\"", echo.getArgument().get(0));
  }

  @Test
  public void testValidArgument() {
    Echo echo = new Echo("echo", args1);
    assertTrue(echo.isValidArgument(current));
  }

  @Test
  public void testExecute() {
    Echo echo = new Echo("echo", args1);
    assertEquals("It should be: ", "a", echo.execute());
  }

}
