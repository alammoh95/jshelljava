package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import driver.Cp_Mv;
import driver.Directory;

public class Cp_MvTest {
  
  ArrayList<String> lst = new ArrayList<String>();
  Directory root = new Directory("/");
  Directory current = root;
  Directory parent = new Directory("");

  @Before
  public void setUp(){
    root.setParentDirectory(parent);
    Directory a = new Directory("a");
    Directory b = new Directory("b");
    root.addDirectory(a);
    root.addDirectory(b);
    lst.add("/a");
    lst.add("/b");
  }

  @Test
  public void testCp_Mv() {
    Cp_Mv cp_mv = new Cp_Mv("cp", lst);
    assertEquals("It should be: ", "cp", cp_mv.getCommand());
    assertEquals("It should be: ", "/a", cp_mv.getArgument().get(0));
    cp_mv = new Cp_Mv("mv", lst);
    assertEquals("It should be: ", "mv", cp_mv.getCommand());
    assertEquals("It should be: ", "/a", cp_mv.getArgument().get(0));

  }

  @Test
  public void testValidArgument() {
    Cp_Mv cp_mv = new Cp_Mv("cp", lst);
    assertTrue(cp_mv.isValidArgument(current));

  }

  @Test
  public void testGetDirectory1() {
    Cp_Mv cp_mv = new Cp_Mv("cp", lst);
    cp_mv.isValidArgument(current);
    ArrayList<String> a2 = new ArrayList<String>();
    a2.add("a");
    assertEquals("It should be: ", a2, cp_mv.getPathDirectory1());
  }

  @Test
  public void testGetDirectory2() {
    Cp_Mv cp_mv = new Cp_Mv("cp", lst);
    cp_mv.isValidArgument(current);
    ArrayList<String> a2 = new ArrayList<String>();
    a2.add("b");
    assertEquals("It should be: ", a2, cp_mv.getPathDirectory2());
  }

  @Test
  public void testExecuteCp() {
    Cp_Mv cp_mv = new Cp_Mv("cp", lst);
    cp_mv.isValidArgument(current);
    cp_mv.executeCp(root);
    assertEquals("It should be: ", "a", 
        ((Directory)root.getChildren().get(1)).getChildren().get(0).getName());

  }

  @Test
  public void testExecuteMv() {
    Cp_Mv cp_mv = new Cp_Mv("mv", lst);
    cp_mv.isValidArgument(current);
    cp_mv.executeMv(root, current);
    assertEquals("It should be: ", "b", root.getChildren().get(0).getName());
  }

}

