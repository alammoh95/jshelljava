package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

import driver.Directory;
import driver.FileName;

public class FileNameTest {

  @Test
  public void testFileName() {
    FileName f1=new FileName("Hello");
    assertEquals("It should be: ", "Hello", f1.getName() );
  }
  
  @Test
  public void testRename(){
    FileName f=new FileName("A");
    f.rename("B");
    assertEquals("It should be: ", "B", f.getName());
  }
  
  @Test
  public void testGetName(){
    FileName f= new FileName("A");
    assertEquals("It should be: ", "A", f.getName());
  }
  
  @Test
  public void testSameName(){
    FileName f= new FileName("A");
    FileName f1= new FileName("A");
    assertTrue(f.sameName(f1));
    f1.rename("C");
    assertFalse(f.sameName(f1));
  }
  
  @Test
  public void testParetnDirectory(){
    Directory a = new Directory("A");
    Directory b = new Directory("B");
    a.setParentDirectory(b);
    assertEquals("B", a.getParentDirectory().getName());
  }
  
}
