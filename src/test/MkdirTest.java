package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import driver.Cd;
import driver.Directory;
import driver.Ls;
import driver.Mkdir;

public class MkdirTest {

  public ArrayList<String> arg = new ArrayList<String>();
  public Directory current = new Directory("/");
  public Directory root = current;
  public Directory parent = new Directory("");
  public Directory a = new Directory("a");
  public Directory b = new Directory("b");

  @Before
  public void setUp(){
    arg.add("c");
    root.setParentDirectory(parent);
    a.addDirectory(b);
    root.addDirectory(a);
  }
  
  @Test
  public void testMkdir() {
    Mkdir mkdir = new Mkdir("mkdir", arg);
    assertEquals("It should be: ", "mkdir", mkdir.getCommand());
    assertEquals("It should be: ", "c", mkdir.getArgument().get(0));
  }

  @Test
  public void testIsValidArgument() {
    Mkdir mkdir = new Mkdir("mkdir", arg);
    assertTrue(mkdir.isValidArgument(current));
  }

  @Test
  public void testGetDirectory() {
    Mkdir mkdir = new Mkdir("mkdir", arg);
    mkdir.isValidArgument(current);
    ArrayList<String> lst = new ArrayList<String>();
    lst.add("c");
    assertEquals("It should be: ", lst, mkdir.getlistPathDirectory().get(0));
  }

  @Test
  public void testExecute() {
    Mkdir mkdir = new Mkdir("mkdir", arg);
    mkdir.isValidArgument(current);
    mkdir.execute(root);
    assertEquals("c", root.getChildren().get(1).getName());
   }

}
