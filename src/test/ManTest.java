package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import driver.Man;

public class ManTest {

  public ArrayList<String> a1 = new ArrayList<String>();

  @Test
  public void testMan() {
    a1.add("pwd");
    Man man = new Man("man", a1);
    assertEquals("It should be: ", "man", man.getCommand());
    assertEquals("It should be: ", "pwd", man.getArgument().get(0));

  }

  @Test
  public void testValidArgument() {
    a1.add("ls");
    Man man = new Man("man", a1);
    assertTrue(man.isValidArgument(null));
  }

  @Test
  public void testPrintDoc() {
    a1.clear();
    a1.add("pwd");
    Man man = new Man("man", a1);
    assertEquals("It should be: ", "pwd -- Print the working directory.",
        man.printDoc(man.getArgument().get(0)));
  }

}
