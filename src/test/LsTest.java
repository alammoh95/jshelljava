package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import driver.Cd;
import driver.Directory;
import driver.File;
import driver.Ls;

public class LsTest {

  public ArrayList<String> arg = new ArrayList<String>();
  public Directory current = new Directory("/");
  public Directory root = current;
  public Directory parent = new Directory("");
  public Directory a = new Directory("a");
  public Directory b = new Directory("b");

  @Before
  public void setUp(){
    root.setParentDirectory(parent);
    a.addDirectory(b);
    root.addDirectory(a);
  }
  
  @Test
  public void testLs() {
    Ls ls = new Ls("ls", arg);
    assertEquals("Command should be: ", "ls", ls.getCommand());
    assertEquals("Argument should be: ", arg, ls.getArgument());
  }

  @Test
  public void testValidArgument() {
    Ls ls = new Ls("ls", arg);
    assertTrue(ls.isValidArgument(current));
  }

  @Test
  public void testGetPathDirectory() {
    Ls ls = new Ls("ls", arg);
    ls.isValidArgument(current);
    ArrayList<String> lst = new ArrayList<String>();
    assertEquals("It should be: ", lst, ls.getListPathDirectory());
  }
  
  @Test
  public void testRecursivePrint(){
    Ls ls = new Ls("ls", arg);
    ls.isValidArgument(current);
    String s = "/:   a   \n/a:   b   \n/a/b:   \n";
    assertEquals(s, ls.recursivePrint(root));
  }

  @Test
  public void testExecute() {
    Ls ls = new Ls("ls", arg);
    ls.isValidArgument(current);
    assertEquals("a    ", ls.execute(root, current, false));
  }

}
