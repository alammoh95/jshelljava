package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import driver.Directory;
import driver.File;
import driver.Redirection;

public class RedirectionTest {

  public ArrayList<String> pathDirectory = new ArrayList<String>();
  public Directory current = new Directory("/");
  public Directory root = current;
  public Directory parent = new Directory("");
  public File atxt = new File("atxt");
  
  @Before
  public void setUp(){
    atxt.setContent("def");
    root.addFile(atxt);
    pathDirectory.add("atxt");
  }
  
  @Test
  public void testRedirection() {
    Redirection x = new Redirection("abc", ">>", pathDirectory);
    assertEquals("abc", x.getOutput());
    assertEquals(">>", x.getArrow());
    assertEquals(pathDirectory, x.getPathDirectory());
  }

  @Test
  public void testExecute() {
    Redirection x = new Redirection("abc", ">", pathDirectory);
    x.execute(root);
    assertEquals("abc", atxt.getContent().get(0));
    Redirection y = new Redirection("abc", ">>", pathDirectory);
    y.execute(root);
    assertEquals("abc", atxt.getContent().get(1));
  }
  
  @Test
  public void testGetOutput(){
    Redirection x = new Redirection("abc", ">", pathDirectory);
    assertEquals("abc", x.getOutput());
  }

  @Test
  public void testGetArrow(){
    Redirection x = new Redirection("abc", ">", pathDirectory);
    assertEquals(">", x.getArrow());
  }
  
  @Test
  public void testGetPathDirectory(){
    Redirection x = new Redirection("abc", ">", pathDirectory);
    assertEquals(pathDirectory, x.getPathDirectory());
  }
  
}
