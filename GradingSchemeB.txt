Marked by Raymond Lei, Please contact yunyunsin@gmail.com if you have any questions regarding your marks.
==== A2 phase I marking ====
tahirha3 will failing mark due to ZERO contribution in this part: F

Group number: 93
UTORID usernames of members:
// UTOR user_name1: chowdhr8
// UTOR user_name2: pushi
// UTOR user_name3: alammoh5
// UTOR user_name4: tahirha3
//
// Author1: Mohammad Chowdhry
// Author2: Shi Pu
// Author3: Mohammad Osama Alam
// Author4: Hassan Ali Tahir
Total deductions: 0
Grade: A+

Your team's Phase II grade is based on this scale:

    http://www.artsandscience.utoronto.ca/ofr/calendar/rules.htm#grading

The way we graded is as follows: your team starts with an A+. When errors and
problems are found, you will lose 0, 1, 2, or 3 letter steps, depending on the
severity. As an example, if you lost two steps, your team would earn an A-:

     A+ -> A
     A -> A-

You were asked to follow the Scrum process. A big part of that is to maintain
a product backlog and a sprint backlog. 

==== Product and Sprint backlogs ==== [up to -6]

Several steps can be lost for these kinds of issues:


==== Java Style ==== [up to -6]

Steps can be lost for these kinds of issues:


==== Design ==== [up to -6]

Several steps can be lost for these kinds of issues:


==== Javadoc ==== [up to -3]

Several steps can be lost for these kinds of issues:


==== Testing ==== [up to -6] 
Several steps can be lost for these kinds of issues:


==== Correctness ==== [up to -6]

	grep doesn't really work hence no bonus is awarded. Everything else worked, good!
    
