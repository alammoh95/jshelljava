Class name: Cp_Mv

Parent class: AbstractCommand
Subclasses: N/A

Responsibilities:
* Check if the argument for cp and mv commmand is valid or not
* Moves or copy a file or a directory from one directory to another

Collaborators:
* AbstractCommand
* Directory
* File