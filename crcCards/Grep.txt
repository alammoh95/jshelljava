Class name: Grep

Parent class: AbstractCommand
Subclasses: N/A

Responsibilities:
* Check if the argument for Grep commmand is valid or not
* Find contents of files in file system that contain match of the given regex
* Add content to a file

Collaborators:
* AbstractCommand
* Directory
* File