Class name: File

Parent class: N/A
Subclasses: TextFile, Directory

Responsibilities:

* Rename a file
* Copy a file
* Add content to a file
* Get the content of a file

Collaborators:
* FileName