Class name: JShell

Parent class: N/A
Subclasses: N/A

Responsibilities:
* Start a shell that allows the user to interact with an initially empty file 
system.
* Exit the shell.
* Set the root directory
* Track the working directory
* Check if the command is valid


Collaborators:
* Cd
* Cp_Mv
* Echo
* Ls
* Man
* Mkdir
* Pwd
* Cat
* Rm
* Grep
* GetUrl
* Directory
* File
* Redirection