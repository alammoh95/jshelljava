October 20 to October 23:

Product Backlog items to be implemented:
a) Add Crc Cards
b) Create classes for CRC Cards
c) Prompt user for input
d) Handle input
e) Display Output

Specific tasks to be accomplished:
a-1) Add Crc Cards for File, Directory, JShell
b-1) (Directory) Write the base code for the Directory class
b-2) (File) Write the base code for the File class
b-3) (JShell) Write the base code for the JShell class
c-1) (JShell) Have the shell continually prompt the user for input
d-1) (JShell) Parse the user input for proper keywords
e-1) (JShell) Print Console

These tasks are assigned as follows:

User: chowdhr8
Tasks: b-2

User: alammoh5
Tasks: b-1

User: tahirha3
Tasks: a-1

User: pushi
Tasks: b-3,d-1, e-1 
